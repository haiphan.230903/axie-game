# Axie Game



## Getting started

This is a game made by team GDC (include Phan Thanh Hai, Doan Duy Tung, Nguyen Thai Tan) to submit for Axie Game Jam. Made in 2 weeks.

## Link

- [ ] [Video demo](https://youtu.be/2xus4qR1rQ8)
- [ ] [Game build WebGL](https://haiphan2309.itch.io/axie-rescue-quest)