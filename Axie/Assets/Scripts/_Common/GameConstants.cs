using UnityEngine;
using System;
using GDC.Enums;
using GDC.Common;

namespace GDC.Constants
{
    public class GameConstants
    {
        public const string PLACEABLE_TAG = "Placeable";
        public const string UNPLACEABLE_TAG = "Unplaceable";

        public const int MAX_LEVEL = 4;
        public const int MAX_AXIE = 6;
    }
}
