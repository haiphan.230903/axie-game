using DG.Tweening;
using GDC.Enums;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public static MainMenuController Instance { get; private set; }
    [SerializeField] RectTransform startMenu, chooseMenu, blackTop, blackBottom;
    [SerializeField] Transform axie;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        SoundManager.Instance.LoadSoundMap(SoundType.MAIN_MENU);
    }
    public void ReturnChoosingMenu()
    {
        chooseMenu.anchoredPosition = Vector2.zero;
        axie.position = new Vector3(-5f, 8.45f, 0);
        startMenu.anchoredPosition = new Vector2(-1920, 0);
        blackTop.anchoredPosition = new Vector2(0, -70);
        blackBottom.anchoredPosition = new Vector2(0, 70);
    }
    public void StartButton()
    {
        chooseMenu.DOAnchorPosX(0, 0.5f).SetEase(Ease.OutBack);
        axie.DOMoveX(-5, 0.5f).SetEase(Ease.OutBack);
        startMenu.DOAnchorPosX(-1920, 0.5f);
        blackTop.DOAnchorPosY(-70, 0.5f);
        blackBottom.DOAnchorPosY(70, 0.5f);
    }
    public void CreditButton()
    {

    }
    public void BackButton()
    {
        chooseMenu.DOAnchorPosX(1920, 0.5f);
        axie.DOMoveX(20, 0.5f);
        startMenu.DOAnchorPosX(0, 0.5f).SetEase(Ease.OutBack);
        blackTop.DOAnchorPosY(150, 0.5f);
        blackBottom.DOAnchorPosY(-150, 0.5f);
    }
    public void SettingButton()
    {

    }
    public void GoButton()
    {
        if (SaveLoadManager.Instance.GameData.currentLevelIndex>SaveLoadManager.Instance.GameData.levelIndex)
        {
            Debug.Log("Level is locking!");
        }
        else
        {
            Debug.Log("Enter level " + SaveLoadManager.Instance.GameData.currentLevelIndex + 1);
            SoundType soundMapType = SoundType.GAMEPLAY1;
            SceneType sceneType = SceneType.LEVEL_1;
            if (SaveLoadManager.Instance.GameData.currentLevelIndex > 1) soundMapType = SoundType.GAMEPLAY2;
            if (SaveLoadManager.Instance.GameData.currentLevelIndex == 0) sceneType = SceneType.LEVEL_1;
            else if (SaveLoadManager.Instance.GameData.currentLevelIndex == 1) sceneType = SceneType.LEVEL_2;
            else if (SaveLoadManager.Instance.GameData.currentLevelIndex == 2) sceneType = SceneType.LEVEL_3;
            else sceneType = SceneType.LEVEL_4;
            GameManager.Instance.LoadSceneAsyncManually(
                sceneType,
                TransitionType.IN,
                soundMapType,
                cb: () =>
                {
                    GameManager.Instance.UnloadSceneManually(
                        SceneType.MAIN
                        //cb: () => GameManager.Instance.SetInitData()
                    );

                },
                true);
        } 
            
    }
}
