using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class Credit : MonoBehaviour
    {
        [SerializeField] Transform credit;
        [SerializeField] Image closeButton;

        public void Show()
        {
            gameObject.SetActive(true);
            credit.DOScale(1, 0.5f).SetEase(Ease.OutBack);
        }

        public void Hide()
        {
            closeButton.raycastTarget = false;
            credit.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(() =>
            {
                closeButton.raycastTarget=true;
                gameObject.SetActive(false);
            });
        }
    }
}