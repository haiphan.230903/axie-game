using DG.Tweening;
using GDC.Gameplay.Player;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Build;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class Heart : MonoBehaviour
    {
        [SerializeField,ReadOnly]List<Transform> hearts = new List<Transform>();
        public List<Transform> Hearts { get => hearts; }
        [SerializeField,InfoBox("= hp")] int heartNumber;
        public static Heart Instance { get; private set; }
        private void Awake()
        {
            Instance = this;
        }

        [Button]
        public void SpawnHeart()
        {

            if (heartNumber < 1)
                return;
            hearts.Clear();

            GameObject template = Instantiate(transform.GetChild(0).gameObject);
            while (transform.childCount > 0)
                DestroyImmediate(transform.GetChild(0).gameObject);

            for (int i =0; i < heartNumber; i++)
            {
                GameObject clone = Instantiate(template,transform);
                clone.name = "HeartBG";
                clone.GetComponent<RectTransform>().anchoredPosition = new Vector2(100 * i, 0);
                hearts.Add(clone.transform.GetChild(0));
            }

            DestroyImmediate(template);
        }

        public void Hurt(int currentHealth)
        {
            hearts[currentHealth].transform.DOScale(0, 0.5f).SetEase(Ease.InBack);
        }

        public void Heal(int currentHealth)
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HEAL);
            hearts[currentHealth].transform.DOScale(1, 0.5f).SetEase(Ease.OutBack);
        }

        public void SetUp()
        {
            for (int i = 0; i < hearts.Count; i++)
            {
                Heal(i);
            }
        }
    }
}