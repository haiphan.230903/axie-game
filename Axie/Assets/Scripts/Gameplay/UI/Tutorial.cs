using DG.Tweening;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    [SerializeField] Image closeButton;
    public void Close()
    {
        SaveLoadManager.Instance.GameData.isSawTutorial = true;
        closeButton.raycastTarget = false;
        transform.DOScale(0, 0.5f).SetUpdate(true).SetEase(Ease.InBack).OnComplete(()=>
        {
            closeButton.raycastTarget = true;
            Time.timeScale = 1;
        }
        );
    }

    public void Show()
    {
        if (SaveLoadManager.Instance.GameData.isSawTutorial) return;
        Time.timeScale = 0;
        transform.DOScale(1, 0.5f).SetUpdate(true).SetEase(Ease.OutBack);
    }   
}
