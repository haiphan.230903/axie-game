using DG.Tweening;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class CustomButton : MonoBehaviour
    {
        [SerializeField] Image[] images;
        [SerializeField] RectTransform cloudRect, treeRect;
        [SerializeField] RectTransform button;
        [SerializeField] AnimationCurve animCurve;
        [SerializeField] float rotateMax, rotateDuration;
        [SerializeField] TMP_Text nameText;
        Tween animTween;
        List<Color> imagesColor;
        public bool isDisable;
        void Start()
        {
            imagesColor = new();
            foreach (Image image in images)
            {
                imagesColor.Add(image.color);
            }
            SetUp();
        }

        public void SetUp()
        {
            foreach (Image image in images)
            {
                image.color = new Color(image.color.r, image.color.g, image.color.b, 0.5f);
            }

            cloudRect.anchoredPosition = new Vector2(-300,cloudRect.anchoredPosition.y);
            treeRect.anchoredPosition = new Vector2(300,treeRect.anchoredPosition.y);
            nameText.color = new Color(nameText.color.r, nameText.color.g, nameText.color.b, 0.5f);
            transform.localScale = new Vector3(0.8f,0.8f,0.8f);

            Delay();
        }

        void Delay()
        {
            animTween?.Kill();
            animTween = button.DOLocalRotate(new Vector3(0, 0, rotateMax), rotateDuration).SetEase(animCurve).SetLoops(-1).SetUpdate(true);
            animTween.Play();
        }

        public void EnterButton()
        {
            if (isDisable) return;
            foreach (Image image in images)
            {
                image.DOFade(1, 0.5f).SetUpdate(true);
            }
            //float distance = treeRect.anchoredPosition.x;
            //SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            cloudRect?.DOAnchorPosX(0, 2).SetUpdate(true);
            treeRect?.DOAnchorPosX(0, 2).SetUpdate(true);
            nameText.DOFade(1f, 0.5f).SetUpdate(true);
            animTween?.Kill();
            animTween = button.DOLocalRotate(Vector3.zero, 0.5f).SetUpdate(true);
            animTween.Play();
            transform.DOScale(1, 0.5f).SetEase(Ease.OutBack).SetUpdate(true);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.BUTTON_HOVER);
        }

        public void ExitButton()
        {
            if (isDisable) return;
            foreach (Image image in images)
            {
                image.DOFade(0.5f, 0.5f).SetUpdate(true);
            }

            cloudRect?.DOAnchorPosX(-300, 2).SetUpdate(true);
            treeRect?.DOAnchorPosX(300, 2).SetUpdate(true);
            nameText.DOFade(0.8f, 0.5f).SetUpdate(true);
            transform.DOScale(0.8f, 0.5f).SetUpdate(true);

            Delay();
        }

        public void ButtonDown()
        {
            if (isDisable) return;
            //SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_PRESS_BUTTON);
            for (int i=0; i<images.Length; i++)
            {
                Color shadowColor = imagesColor[i] / 2;
                shadowColor.a = 1;
                images[i].color = shadowColor;
            }
            DOTween.Kill(transform);
            transform.localScale = Vector3.one * 0.8f;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.BUTTON_CLICK);
        }
        public void ButtonUp()
        {
            if (isDisable) return;
            // foreach (Image image in images)
            // {
            //     image.color = Color.white;
            // }
            for (int i=0; i<images.Length; i++)
            {
                images[i].color = imagesColor[i];
            }
            transform.localScale = Vector3.one;
        }
    }
}