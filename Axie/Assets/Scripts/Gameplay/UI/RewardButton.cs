using DG.Tweening;
using GDC.Gameplay.Player;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class RewardButton : MonoBehaviour
    {
        [SerializeField] Transform button;
        [SerializeField] Image bg;
        [SerializeField] Image graphic;
        [SerializeField] Animator bgAnim;
        [SerializeField] TMP_Text text;
        RewardID rewardID;
        bool isClick = false;

        public void SetUp()
        {
            button.localScale = Vector3.zero;
            graphic.color = Color.clear;
            bgAnim.speed = 1;
        }

        public void Show(Sprite icon, Color color,string description,RewardID id)
        {
            rewardID = id;
            graphic.sprite = icon;
            graphic.color = new Color(1, 1, 1, 0.5f);
            bg.color = new Color(color.r, color.g, color.b, 0.5f);
            text.color = new Color(1, 1, 1, 0.5f);
            text.text = description;
            button.DOScale(1.2f, 0.5f).SetUpdate(true).OnComplete(()=>bg.raycastTarget = true);
            bgAnim.speed = 1;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_LEVEL_UP);
        }

        public void Hide()
        {
            isClick = true;
            button.DOScale(0, 0.5f).SetUpdate(true).OnComplete(() => isClick = false);
        }

        public void OnEnter()
        {
            if (isClick)
                return;
            graphic.DOFade(1, 0.5f).SetUpdate(true);
            bg.DOFade(1, 0.5f).SetUpdate(true);
            button.DOScale(1.5f, 0.5f).SetUpdate(true);
            bgAnim.speed = 2;
            text.DOFade(1, 0.5f).SetUpdate(true);
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.BUTTON_HOVER);
        }

        public void OnExit()
        {
            if (isClick)
                return;
            graphic.DOFade(0.5f, 0.5f).SetUpdate(true);
            bg.DOFade(0.5f, 0.5f).SetUpdate(true);
            button.DOScale(1.2f, 0.5f).SetUpdate(true);
            bgAnim.speed = 1;
            text.DOFade(0.5f, 0.5f).SetUpdate(true);
        }

        public void OnClick()
        {
            if (isClick)
                return;
            //isClick = true;
            bg.raycastTarget = false;
            switch (rewardID)
            {
                
                case RewardID.HEAL:
                    Player.Player.Instance.CurrentHP++; break;
                case RewardID.BUFF_ATTACK_SPEED:
                    Player.Player.Instance.AttackSpeed*=1.5f ; break;
                case RewardID.BUFF_SPEED:
                    Player.Player.Instance.Speed*=1.3f; break;
                case RewardID.BUFF_SLASH:
                    Player.Player.Instance.BulletScale *= 1.5f; break;
                case RewardID.SPIKE:
                    Player.Player.Instance.SpellDict[SpellID.SPIKE].remainTime +=2;
                    SpellDisplay.Instance.UpdateSpell(SpellID.SPIKE,Player.Player.Instance.SpellDict[SpellID.SPIKE].remainTime);
                    break;
                case RewardID.FIRE:
                    Player.Player.Instance.SpellDict[SpellID.FIRE].remainTime ++;
                    SpellDisplay.Instance.UpdateSpell(SpellID.FIRE, Player.Player.Instance.SpellDict[SpellID.FIRE].remainTime);
                    break;
                case RewardID.BEAM:
                    Player.Player.Instance.SpellDict[SpellID.BEAM].remainTime +=2; 
                    SpellDisplay.Instance.UpdateSpell(SpellID.BEAM, Player.Player.Instance.SpellDict[SpellID.BEAM].remainTime);
                    break;
            }
            RewardPanel.Instance.Hide();
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.BUTTON_CLICK);
        }
 
    }
}