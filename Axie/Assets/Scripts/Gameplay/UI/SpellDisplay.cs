using GDC.Gameplay.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class SpellDisplay : MonoBehaviour
    {
        public static SpellDisplay Instance { get; private set; }

        [SerializeField] List<SpellSlot> slots = new List<SpellSlot>();

        private void Awake()
        {
            Instance = this;
        }

        public void SetUp()
        {
            SpellDict spellDict = Player.Player.Instance.SpellDict;

            foreach (SpellSlot slot in slots)
                slot.SetUp(spellDict[slot.SpellID].remainTime);
        }

        public void UpdateSpell(SpellID spellID, int remainTime)
        {
            foreach (SpellSlot slot in slots)
                if (slot.SpellID == spellID)
                {
                    slot.SetUp(remainTime);
                    break;
                }
        }
    }
}