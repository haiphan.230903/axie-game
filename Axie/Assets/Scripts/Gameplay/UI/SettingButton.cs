using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using GDC.Managers;
using UnityEngine.UI;
using GDC.Enums;

namespace GDC.Gameplay.UI
{
    public class SettingButton : MonoBehaviour
    {
        [SerializeField] RectTransform rectTransform;
        [SerializeField] RectTransform settingPanel, toMainMenuPanel, quitPanel, guidePanel, creditPanel;
        bool isOpening;
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnClick();
            }
        }
        public void OnEnter()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HOLD_ITEM);
            rectTransform.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 0.5f).SetUpdate(true);
        }    
        public void OnExit()
        {
            rectTransform.DOScale(Vector3.one, 0.5f).SetUpdate(true);
        }
        public void OnClick()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_PRESS_BUTTON);      
            if (!isOpening)
            {
                // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_START_UI);
                settingPanel.DOAnchorPosY(0, 0.5f).SetUpdate(true);
                isOpening = true;
                Time.timeScale = 0;
            }
            else OnClickExitSettingPanel();
        }
        public void OnClickExitSettingPanel()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_END_UI);
            Time.timeScale = 1;

            settingPanel.DOAnchorPosY(-1100, 0.5f).SetUpdate(true);
            toMainMenuPanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
            quitPanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
            guidePanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
            
            isOpening = false;
        }
        public void OnClickToMainMenuButton()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            toMainMenuPanel.DOAnchorPosY(0, 0.5f).SetUpdate(true);
        }
        public void OnClickQuitButton()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            quitPanel.DOAnchorPosY(0, 0.5f).SetUpdate(true);
        }
        public void OnClickGuideButton()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            guidePanel.gameObject.SetActive(true);
            guidePanel.DOAnchorPosY(0, 0.5f).SetUpdate(true);
        }
        public void YesQuitGame()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            Application.Quit();
            print("QUIT");
        }
        public void NoQuitGame()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            quitPanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
        }
        public void YesToMainMenu()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            print("TO MAIN MENU");
            Time.timeScale = 1;
            GameManager.Instance.LoadSceneAsyncManually(
                SceneType.MAIN,
                TransitionType.IN,
                SoundType.MAIN_MENU,
                cb: () =>
                {
                    int levelIndex = SaveLoadManager.Instance.GameData.currentLevelIndex;
                    if (levelIndex == 0)
                    {
                        GameManager.Instance.UnloadSceneManually(
                            SceneType.LEVEL_1,
                            cb: () => GameManager.Instance.SetInitData()
                        );
                    }
                    else if (levelIndex == 1)
                    {
                        GameManager.Instance.UnloadSceneManually(
                            SceneType.LEVEL_2,
                            cb: () => GameManager.Instance.SetInitData()
                        );
                    }
                    if (levelIndex == 2)
                    {
                        GameManager.Instance.UnloadSceneManually(
                            SceneType.LEVEL_3,
                            cb: () => GameManager.Instance.SetInitData()
                        );
                    }
                    if (levelIndex == 3)
                    {
                        GameManager.Instance.UnloadSceneManually(
                            SceneType.LEVEL_4,
                            cb: () => GameManager.Instance.SetInitData()
                        );
                    }
                },
                true);
        }
        public void NoToMainMenu()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            toMainMenuPanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
        }
        public void OnClickExitToMainMenuPanel()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            toMainMenuPanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
        }
        public void OnClickExitGuidePanel()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            guidePanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
        }
        public void OnClickCreditButton()
        {
            // SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_RELEASE_ITEM);
            creditPanel.DOAnchorPosY(0, 0.5f).SetUpdate(true);
        }
        public void OnClickExitCreditPanel()
        {
            creditPanel.DOAnchorPosY(-1150, 0.5f).SetUpdate(true);
        }
    }
}
