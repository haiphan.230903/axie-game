using DG.Tweening;
using GDC.Gameplay.Player;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{

    public class SpellSlot : MonoBehaviour
    {
        [SerializeField] Image icon;
        [SerializeField] TMP_Text remainTimeText;
        [SerializeField] SpellID spellID;
        public SpellID SpellID => spellID;  


        public void SetUp(int remainTime)
        {
            remainTimeText.text = remainTime.ToString();
            if (remainTime == 0)
                icon.color = new Color(1, 1, 1, 0.3f);
            else
            {
                transform.DOScale(1.5f, 0.5f).SetLoops(4, LoopType.Yoyo);
                icon.color = Color.white;
            }    
        }
    }
}