using DG.Tweening;
using GDC.Managers;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxieChoosingButton : MonoBehaviour
{
    [SerializeField] Transform question, circle;
    [SerializeField] SkeletonAnimation axieSkeAnim;
    [SerializeField]
    [SpineAnimation]
    private string _idleAnimName;
    [SerializeField]
    [SpineAnimation]
    private string _choosingAnimName;
    public bool isChoosing, isLock;
    public int axieIndex;
    private void OnMouseEnter()
    {
        if (isLock) return;
        circle.DOScale(2, 0.5f).SetEase(Ease.OutBack);
        axieSkeAnim.AnimationName = _choosingAnimName;
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.BUTTON_HOVER);
    }
    private void OnMouseUpAsButton()
    {
        if (isLock)
        {
            question.DOScale(0.2f, 0.25f).SetLoops(2, LoopType.Yoyo);
        }
        else
        {
            Choose();
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.BUTTON_CLICK);
        }
    }
    private void OnMouseExit()
    {
        if (isChoosing) return;

        circle.DOScale(0, 0.5f);
        axieSkeAnim.AnimationName = _idleAnimName;
    }
    public void Choose(bool isImidiately = false)
    {
        
        AxieChoosing.Instance.UnChooseAll();
        isChoosing = true;
        if (isImidiately)
        {
            DOTween.Kill(circle);
            circle.localScale = new Vector3(2, 2, 2);
        }
        else
        {
            circle.DOScale(2.5f, 0.25f).SetLoops(2, LoopType.Yoyo);
        }
        axieSkeAnim.AnimationName = _choosingAnimName;
        SaveLoadManager.Instance.GameData.currentAxieIndex = axieIndex;
    }
    public void UnChoose()
    {
        isChoosing = false;
        circle.DOScale(0, 0.5f);
        axieSkeAnim.AnimationName = _idleAnimName;
    }
    public void Setup(bool isLock)
    {
        this.isLock = isLock;
        if (isLock)
        {
            axieSkeAnim.gameObject.SetActive(false);
            question.gameObject.SetActive(true);
        }
        else
        {
            axieSkeAnim.gameObject.SetActive(true);
            question.gameObject.SetActive(false);
        }
    }
}
