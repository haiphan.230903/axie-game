using DG.Tweening;
using NaughtyAttributes;
using RotaryHeart.Lib.SerializableDictionary;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public enum RewardID
    {
        HEAL,
        BUFF_ATTACK_SPEED,
        BUFF_SPEED,
        BUFF_SLASH,
        SPIKE,
        FIRE,
        BEAM,
    }

    [System.Serializable]
    class RewardInfo
    {
        public Sprite sprite;
        public Color color;
        public string description;
    }

    [System.Serializable]
    class RewardDict : SerializableDictionaryBase<RewardID, RewardInfo> { }
    public class RewardPanel : MonoBehaviour
    {
        public static RewardPanel Instance { get; private set; }

        [SerializeField] RewardDict rewardDict;
        [SerializeField] Image background;
        [SerializeField] List<RewardButton> rewardButtons = new List<RewardButton>();
        public bool isShowing;

        private void Awake()
        {
            Instance = this;
        }

        public void SetUp()
        {
            

            background.color = new Color(background.color.r, background.color.g, background.color.b, 0);
            foreach (RewardButton button in rewardButtons)
            {
                button.SetUp();
            }
        }

        [Button]
        public void Show()
        {
            Time.timeScale = 0;
            isShowing = true;
            int enumCount = 7;
            List<int> pickUpList = new List<int>();
            for (int i = 0; i < enumCount; i++)
                pickUpList.Add(i);

            background.DOFade(0.5f, 0.5f).SetUpdate(true);
            foreach (RewardButton button in rewardButtons)
            {
                int rewardNum = pickUpList[Random.Range(0, pickUpList.Count)];
                pickUpList.Remove(rewardNum);
                RewardID rewardID = (RewardID)rewardNum;

                button.Show(rewardDict[rewardID].sprite, rewardDict[rewardID].color, rewardDict[rewardID].description, rewardID);
            }
        }

        public void Hide()
        {
            foreach (RewardButton button in rewardButtons)
                button.Hide();
            background.DOFade(0, 0.5f).SetUpdate(true).OnComplete(() => 
            {
                isShowing = false;
                Time.timeScale = 1;
            });

        }
    }
}