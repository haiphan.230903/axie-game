using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxieChoosing : MonoBehaviour
{
    public static AxieChoosing Instance { get; private set; }
    [SerializeField] List<AxieChoosingButton> axieChoosingButtons;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        StartCoroutine(Cor_Setup());
    }
    IEnumerator Cor_Setup()
    {
        yield return new WaitUntil(() => SaveLoadManager.Instance != null);
        yield return new WaitUntil(() => SaveLoadManager.Instance.isLoadingData);
        Setup();
    }
    public void Setup()
    {
        SaveLoadManager.Instance.GameData.AxieIndex = SaveLoadManager.Instance.GameData.levelIndex + 2;
        foreach (AxieChoosingButton button in axieChoosingButtons)
        {
            if (button.axieIndex <= SaveLoadManager.Instance.GameData.AxieIndex)
            {
                button.Setup(false);
            }
            else
            {
                button.Setup(true);
            }

            if (button.axieIndex == SaveLoadManager.Instance.GameData.currentAxieIndex)
            {
                button.Choose(true);
            }
        }
    }
    public void UnChooseAll()
    {
        foreach (AxieChoosingButton button in axieChoosingButtons)
        {
            button.UnChoose();
        }
    }
}
