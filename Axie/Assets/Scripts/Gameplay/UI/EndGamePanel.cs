using DG.Tweening;
using GDC.Constants;
using GDC.Enums;
using GDC.Gameplay.Player;
using GDC.Managers;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

namespace GDC.Gameplay.UI
{
    public class EndGamePanel : MonoBehaviour
    {
        public static EndGamePanel Instance { get; private set; }
        int currentLevelIndex;
        private void Awake()
        {
            Instance = this;
        }

        [SerializeField] List<RectTransform> stars = new List<RectTransform>();
        [SerializeField] Transform elementContainer;
        [SerializeField] GameObject victoryText, gameOverText;
        [SerializeField] Image background;

        public void SetUp()
        {
            background.color = new Color(background.color.r, background.color.g, background.color.b, 0);
            elementContainer.localScale = Vector3.zero;
        }

        [Button]
        public void Victory()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_VICTORY);
            StartCoroutine(Cor_Victory());
        }

        IEnumerator Cor_Victory()
        {
            List<Transform> hearts = Heart.Instance.Hearts;
            int currentHp = Player.Player.Instance.CurrentHP;
            currentLevelIndex = SaveLoadManager.Instance.GameData.currentLevelIndex;
            SaveLoadManager.Instance.GameData.levelStars[currentLevelIndex] = currentHp;
            SaveLoadManager.Instance.GameData.currentLevelIndex++;
            SaveLoadManager.Instance.GameData.levelIndex = currentLevelIndex + 1;
            if (SaveLoadManager.Instance.GameData.levelIndex>GameConstants.MAX_LEVEL - 1)
            {
                SaveLoadManager.Instance.GameData.levelIndex = GameConstants.MAX_AXIE - 1;
                SaveLoadManager.Instance.GameData.currentLevelIndex--;
            }

            victoryText.SetActive(true);
            gameOverText.SetActive(false);

            background.DOFade(0.5f, 1).SetUpdate(true);
            elementContainer.DOScale(1,1).SetEase(Ease.OutBack).SetUpdate(true);
            yield return new WaitForSeconds(1.5f);

            for (int i=0;i<currentHp;i++)
            {
                hearts[i].DOMove(stars[i].position, 0.45f).SetUpdate(true).OnComplete(() =>
                {
                    hearts[i].GetComponent<Image>().DOFade(0, 0.1f).SetUpdate(true).OnComplete(() =>
                    {
                        stars[i].DOScale(1,0.2f).SetEase(Ease.OutBack).SetUpdate(true);
                    });
                });
                yield return new WaitForSeconds(0.8f);
            }
        }
        [Button]
        public void GameOver()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_GAME_OVER);
            currentLevelIndex = SaveLoadManager.Instance.GameData.currentLevelIndex;
            StartCoroutine(Cor_GameOver());
        }

        IEnumerator Cor_GameOver()
        {
            victoryText.SetActive(false);
            gameOverText.SetActive(true);

            background.DOFade(0.5f, 1).SetUpdate(true);
            elementContainer.DOScale(1, 1).SetEase(Ease.OutBack).SetUpdate(true);
            yield return new WaitForSeconds(1.5f);
        }
        public void HomeButton()
        {
            Debug.Log("HOME");
            Time.timeScale = 1;
            GameManager.Instance.LoadSceneAsyncManually(
                SceneType.MAIN,
                TransitionType.IN,
                SoundType.MAIN_MENU,
                cb: () =>
                {
                    int levelIndex = currentLevelIndex;
                    if (levelIndex == 0)
                    {
                        GameManager.Instance.UnloadSceneManually(
                            SceneType.LEVEL_1,
                            cb: () => GameManager.Instance.SetInitData()
                        );
                    }
                    else if (levelIndex == 1)
                    {
                        GameManager.Instance.UnloadSceneManually(
                            SceneType.LEVEL_2,
                            cb: () => GameManager.Instance.SetInitData()
                        );
                    }
                    if (levelIndex == 2)
                    {
                        GameManager.Instance.UnloadSceneManually(
                            SceneType.LEVEL_3,
                            cb: () => GameManager.Instance.SetInitData()
                        );
                    }
                    if (levelIndex == 3)
                    {
                        GameManager.Instance.UnloadSceneManually(
                            SceneType.LEVEL_4,
                            cb: () => GameManager.Instance.SetInitData()
                        );
                    }
                },
                true);
        }
    }
}