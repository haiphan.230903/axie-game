using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.UI
{
    public class UIGameplay : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            Heart.Instance?.SetUp();
            EndGamePanel.Instance?.SetUp();
            RewardPanel.Instance?.SetUp();
            SpellDisplay.Instance?.SetUp();
        }

    }
}