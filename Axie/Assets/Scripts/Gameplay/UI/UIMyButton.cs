using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using GDC.Managers;

public class UIMyButton : MonoBehaviour
{
    [SerializeField] Image image;
    [SerializeField] Sprite idleSprite, hoverSprite, downSprite;
    
    public void OnEnter()
    {
        if (hoverSprite != null)
            image.sprite = hoverSprite;
        image.rectTransform.DOScale(1.3f, 0.5f).SetUpdate(true);
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.BUTTON_HOVER);
    }
    public void OnExit()
    {
        if (idleSprite != null)
            image.sprite = idleSprite;
        image.rectTransform.DOScale(1, 0.5f).SetUpdate(true);
    }
    public void OnDown()
    {
        if (downSprite != null)
            image.sprite = downSprite;
        
    }
    public void OnUp()
    {
        if (hoverSprite != null)
            image.sprite = hoverSprite;
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.BUTTON_CLICK);
    }
    public void OnClick()
    {
        if (hoverSprite != null)
            image.sprite = hoverSprite;
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.BUTTON_CLICK);
    }
}
