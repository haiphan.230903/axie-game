using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;
using NaughtyAttributes;
using GDC.Gameplay.Player;
using GDC.Managers;

namespace GDC.Enemies
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] bool isBoss;
        public SkeletonAnimation anim, anim1;
        public Rigidbody rb;

        [Header("Stat")]
        [SerializeField] float hp;
        public float Hp
        {
            get => hp;
            set => hp = value;
        }
        [SerializeField] private string dieAnimaion;
        private bool isNotMoveAway;
        protected bool isCanGetHit, isInAction;
        public bool isCanFlip = true, isCanMove, isCanAttack;
        Tween turnAroundTween, turnAroundTween1;
        protected int currentFacing, previousFacing; // -1 is left, 1 is right

        [Header("VFX")]
        [SerializeField] GameObject VFX_DisappearEffect;
        private void Awake()
        {
            isCanMove = false;
            isCanGetHit = true;
            if (isBoss) isNotMoveAway = true;
            else isNotMoveAway = false;
            currentFacing = -1;
            previousFacing = -1;
            isInAction = true;
        }
        protected virtual void Start()
        {
            StartCoroutine(Cor_Delay());
        }
        protected virtual void Update()
        {
            anim1.loop = anim.loop;
            anim1.AnimationName = anim.AnimationName;
            if (Input.GetKeyDown(KeyCode.V))
                TestGetHit();
        }
        IEnumerator Cor_Delay()
        {
            yield return new WaitForSeconds(1);
            isCanMove = true;
            isInAction = false;
        }
        protected void Flip(float destinationX)
        {
            if (isCanFlip) {
                if (destinationX >= transform.position.x) currentFacing = 1;
                else currentFacing = -1;
                if (currentFacing != previousFacing)
                {
                    turnAroundTween?.Kill();
                    turnAroundTween1?.Kill();
                    turnAroundTween = anim.transform.DOScaleX(-currentFacing, 0.25f).Play();
                    turnAroundTween1 = anim1.transform.DOScaleX(currentFacing, 0.25f).Play();
                }
                previousFacing = currentFacing;
            }
        }

        public virtual void GetHit(Vector3 attackPos, bool killTween = true)
        {
            if (!isCanGetHit || hp <= 0) return;
            if (killTween)
            {
                StopAllCoroutines();
                this.DOKill();
                transform.DOKill();
                var y = anim.transform.localScale.y;
                var z = anim.transform.localScale.z;
                anim.transform.localScale = new(-currentFacing, y, z);
                anim1.transform.localScale = new(currentFacing, y, z);
            }
            StartCoroutine(Cor_RefreshGetHit(0.5f));
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_ENEMY_HURT);
            StartCoroutine(Cor_Scale(0.8f, 0.15f));

            hp--;
            if (hp <= 0)
            {
                hp = 0;
                Die();
                anim.loop = false;
                anim.AnimationName = dieAnimaion;
                GetComponent<SlimeMove>()?.ResetTime(0, 0, 0.5f);
                return;
            }

            if (isNotMoveAway == false)
            {
                isCanMove = false;
                anim.loop = false;
                anim.AnimationName = "defense/hit-by-normal";

                GetComponent<SlimeMove>()?.ResetTime(0, 0, 0.5f);
                
                Vector3 offset = transform.position - attackPos;
                offset.y = 0;
                transform.DOMove(transform.position + offset.normalized * 0.5f, 0.2f);
                StartCoroutine(Cor_CanMove(1));
            }
            else
            {
                StartCoroutine(Cor_CanMove(1));
            }
        }
        IEnumerator Cor_RefreshGetHit(float sec)
        {
            isCanGetHit = false;
            isCanFlip = false;
            yield return new WaitForSeconds(sec);
            isCanGetHit = true;
            isCanFlip = true;
        }
        IEnumerator Cor_CanMove(float sec)
        {
            yield return new WaitForSeconds(sec);
            if (Hp > 0) 
            {
                isCanMove = true;
                if (GetComponent<BigBear>() == null)
                {
                    isInAction = false;
                }
            }
        }
        IEnumerator Cor_Scale(float scale, float sec)
        {
            Vector3 currScale = anim.transform.localScale;
            anim.transform.DOScale(scale * currScale, sec);

            Vector3 currScale1 = anim1.transform.localScale;
            anim1.transform.DOScale(scale * currScale1, sec);

            yield return new WaitForSeconds(sec);
            anim.transform.DOScale(currScale, sec);
            anim1.transform.DOScale(currScale1, sec);
        }
        public virtual void Die()
        {
            Debug.Log(gameObject.name + " is died");
            isCanMove = false;
            isCanFlip = false;
            StartCoroutine(Cor_Disappear(1.25f));
        }
        IEnumerator Cor_Disappear(float sec)
        {
            yield return new WaitForSeconds(sec);
            Disappear();
        }
        public virtual void Disappear()
        {
            Debug.Log("Enemy Disappear!");
            Instantiate(VFX_DisappearEffect, transform.position, Quaternion.identity);
            gameObject.SetActive(false);
        }
        // public bool DetectWall(Vector3 direction, float distance)
        // {
        //     int layerMask = 1 << 3;
        //     RaycastHit2D[] hits = new RaycastHit2D[1];
        //     int numHits = Physics2D.RaycastNonAlloc(transform.position, direction, hits, distance, layerMask);
        //     for (int i = 0; i < numHits; i++)
        //     {
        //         if (hits[i].collider.CompareTag("Wall"))
        //             return true;
        //     }
        //     return false;
        // }
        void OnCollisionEnter(Collision col)
        {
            if (col.gameObject.CompareTag("PlayerAttack"))
            {
                GetHit(Player.Instance.transform.position);
            }
            else if (col.gameObject.CompareTag("Finish"))
            {
                Die();
            }
        }
        [Button]
        void TestGetHit()
        {
            GetHit(Player.Instance.transform.position);
        }
    }
}
