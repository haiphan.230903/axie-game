using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Gameplay.Player;
using DG.Tweening;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public class OrangeDog : Enemy
    {
        enum DogType {ORANGE, GRAY};
        [Header("OrangeDog")]
        [SerializeField] DogType dogType;
        [SerializeField] float speed;
        [SerializeField] float timeForPrepare, timeBetweenHit;
        Coroutine dogHit, resetHit;
        void FixedUpdate()
        {
            if (!isCanMove) return;
            if (!isInAction)
                dogHit = StartCoroutine(Cor_DogHit());
        }
        IEnumerator Cor_DogHit()
        {
            isInAction = true;
            anim.loop = true;
            Vector3 destination = Player.Instance.transform.position;
            Flip(destination.x);
            if (dogType == DogType.ORANGE)
            {
                anim.AnimationName = "action/random-01";
            }
            else 
            {
                anim.AnimationName = "battle/get-buff";
            }
            yield return new WaitForSeconds(timeForPrepare);
            float dist = Vector3.Distance(destination, transform.position);
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            transform.DOMove(destination, dist / speed);
            yield return new WaitForSeconds(dist / speed - 0.5f);
            anim.loop = false;
            anim.AnimationName = "battle/get-debuff";
            resetHit = StartCoroutine(Cor_ResetHit());
        }
        IEnumerator Cor_ResetHit()
        {
            yield return new WaitForSeconds(0.5f);
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            yield return new WaitForSeconds(timeBetweenHit);
            isInAction = false;
        }
        [Button]
        public override void Die()
        {
            if (dogHit != null) StopCoroutine(dogHit);
            if (resetHit != null) StopCoroutine(resetHit);
            base.Die();
        }
        // void OnCollisionEnter(Collision collision)
        public override void GetHit(Vector3 attackPos, bool killTween = true)
        {
            base.GetHit(attackPos, killTween);
            if (dogHit != null) StopCoroutine(dogHit);
            if (Hp <= 0) return;

            // previousFacing = currentFacing = -1;
            resetHit = StartCoroutine(Cor_ResetHit());
            anim.loop = false;
            anim.AnimationName = "battle/get-debuff";
        }
    }
}