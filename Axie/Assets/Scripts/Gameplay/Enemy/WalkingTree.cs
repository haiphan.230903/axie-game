using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Gameplay.Player;
using DG.Tweening;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public class WalkingTree : Enemy
    {
        [Header("WalkingTree")]
        [SerializeField] float speed;
        [SerializeField] float minX, maxX, minZ, maxZ;
        Coroutine walking;
        void FixedUpdate()
        {
            if (!isCanMove) return;
            if (!isInAction)
            {
                isInAction = true;
                Vector3 target = new(Random.Range(minX, maxX), 0f, Random.Range(minZ, maxZ));
                Flip(target.x);
                anim.loop = true;
                anim.AnimationName = "action/move-forward";
                transform.DOMove(target, (target - transform.position).magnitude / speed)
                    .OnComplete(() => walking = StartCoroutine(Cor_ResetWalking()));
            }
        }
        IEnumerator Cor_ResetWalking()
        {
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            yield return new WaitForSeconds(1);
            isInAction = false;
        }
        [Button]
        public override void Die()
        {
            base.Die();
            if (walking != null) StopCoroutine(walking);
        }
        public override void GetHit(Vector3 attackPos, bool killTween = true)
        {
            base.GetHit(attackPos, killTween);
            if (Hp > 0)
                walking = StartCoroutine(Cor_ResetWalking());
        }
    }
}