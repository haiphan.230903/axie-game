using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ThinGround : MonoBehaviour
{
    public Blaster[] blasters;
    public void Grow(int moveType = 0) //0: normal, 1: up, 2: sin
    {
        gameObject.SetActive(true);
        StopAllCoroutines();
        DOTween.Kill(transform);
        InitBlaster();
        int targetY = 0;
        if (moveType == 1) targetY = 2;
        transform.DOMoveY(targetY, 1).OnComplete(() =>
        {
            if (moveType == 2) SinMove();
        });
    }
    public void Hide()
    {
        if (blasters[0].isFireBallBlaster == false)
        {
            foreach (var b in blasters)
            {
                b.bullet.Explosion();
            }
        }
        DOTween.Kill(transform);
        transform.DOMoveY(-10, 1);      
        StartCoroutine(Cor_Inactive(1));
    }
    public void SinMove()
    {
        transform.DOMoveY(2, 1.5f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
    }
    public void InitBlaster()
    {
        foreach(var blaster in blasters)
        {
            blaster.Init();
        }
    }

    IEnumerator Cor_Inactive(float sec)
    {
        yield return new WaitForSeconds(sec);
        gameObject.SetActive(false);
    }
}
