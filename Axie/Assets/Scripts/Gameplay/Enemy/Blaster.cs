using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using GDC.Managers;

public class Blaster : MonoBehaviour
{
    [SerializeField] GameObject vfx_shotDust;
    [SerializeField] Transform graphicTransform;
    public Bullet bullet;
    public FireBallRotate fireBall3, fireBall5, fireBallTall3;
    [SerializeField] float minShotDeltaTime, maxShotDeltaTime;
    public float frequenceRate; //0-100

    public bool isFireBallBlaster;

    Coroutine shotCoroutine;
    public void Init()
    {
        StopAllCoroutines();
        shotCoroutine = StartCoroutine(Cor_Shot(Random.Range(minShotDeltaTime, maxShotDeltaTime)));
        transform.localScale = Vector3.one;
    }

    IEnumerator Cor_Shot(float sec)
    {
        yield return new WaitForSeconds(Random.Range(minShotDeltaTime, maxShotDeltaTime));

        if (Random.Range(0, 100) <= frequenceRate)
        {
                graphicTransform.DOScale(1.5f, 0.2f);
                yield return new WaitForSeconds(0.2f);
                graphicTransform.DOScale(1f, 0.2f);
                yield return new WaitForSeconds(0.1f);
            Shot();
        }
        shotCoroutine = StartCoroutine(Cor_Shot(Random.Range(minShotDeltaTime, maxShotDeltaTime)));
    }

    void Shot()
    {
        if (isFireBallBlaster == false)
        {
            if (bullet.gameObject.activeSelf == true) return;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SHOT);
            vfx_shotDust.SetActive(true);
            //Bullet bulletScr = Instantiate(this.bulletObj, transform.position, Quaternion.identity).GetComponent<Bullet>();
            bullet.gameObject.SetActive(true);
            bullet.transform.position = transform.position;
            bullet.transform.rotation = Quaternion.Euler(0, 0, 0);
            bullet.SetDir(transform.rotation.eulerAngles.y * Mathf.Deg2Rad);
        }
        else
        {
                if (Random.Range(0, 100) < 60)
                {
                    if (fireBall3.gameObject.activeSelf == true) return;
                    fireBall3.gameObject.SetActive(true);
                    fireBall3.transform.position = transform.position;
                    fireBall3.transform.rotation = Quaternion.Euler(0, 0, 0);
                    fireBall3.SetDir(transform.rotation.eulerAngles.y * Mathf.Deg2Rad, (Random.Range(1, 100) % 2 * 2 - 1) * 90);
                }
                else if (Random.Range(0, 100) < 80)
                {
                    if (fireBall5.gameObject.activeSelf == true) return;
                    fireBall5.gameObject.SetActive(true);
                    fireBall5.transform.position = transform.position;
                    fireBall5.transform.rotation = Quaternion.Euler(0, 0, 0);
                    fireBall5.SetDir(transform.rotation.eulerAngles.y * Mathf.Deg2Rad, (Random.Range(1, 100) % 2 * 2 - 1) * 90);
                }
                else
                {
                    if (fireBallTall3.gameObject.activeSelf == true) return;
                    fireBallTall3.gameObject.SetActive(true);
                    fireBallTall3.transform.position = transform.position;
                    fireBallTall3.transform.rotation = Quaternion.Euler(0, 0, 0);
                    fireBallTall3.SetDir(transform.rotation.eulerAngles.y * Mathf.Deg2Rad, (Random.Range(1, 100) % 2 * 2 - 1) * 90);
                }
        }    
    }
}
