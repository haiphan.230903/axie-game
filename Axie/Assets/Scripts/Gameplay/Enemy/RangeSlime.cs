using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Gameplay.Player;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public class RangeSlime : Enemy
    {
        [Header("RangeSlime")]
        [SerializeField] float timer;
        float timerHelper;
        [SerializeField] GameObject bullet;
        [SerializeField] Transform bulletTip;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        bool canShoot;
        Coroutine shootEvent;

        protected override void Start()
        {
            base.Start();
            timerHelper = timer;
        }
        protected override void Update()
        {
            base.Update();
            if (Hp > 0)
                Flip(Player.Instance.transform.position.x);
        }
        void FixedUpdate()
        {
            if (!isCanMove) return;
            ShootPoison();
        }
        void ShootPoison()
        {
            var direction = Player.Instance.transform.position - this.transform.position;
            var dist = direction.magnitude;
            if (!canShoot)
            {
                timerHelper -= Time.fixedDeltaTime;
                if (timerHelper <= 0f)
                {
                    canShoot = true;
                    timerHelper = timer;
                }
            }
            if (canShoot && dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer)
            {
                GetComponent<SlimeMove>().slimeCanMove = false;
                shootEvent = StartCoroutine(ShootEvent());
            }
            else if (dist > maxDistanceDetectPlayer || dist < minDistanceDetectPlayer)
            {
                timerHelper = timer;
            }
        }
        IEnumerator ShootEvent()
        {
            canShoot = false;
            GetComponent<SlimeMove>().ResetTime(0f, 0f, 1.5f);
            anim.loop = false;
            anim.AnimationName = "attack/ranged/cast";
            isCanFlip = false;
            yield return new WaitForSeconds(0.5f);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SHOT);
            var go = Instantiate(bullet, bulletTip.position, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(Player.Instance.transform.position - bulletTip.position, false);
            yield return new WaitForSeconds(1);
            isCanFlip = true;
        }
        [Button]
        public override void Die()
        {
            GetComponent<SlimeMove>().enabled = false;
            base.Die();
            if (shootEvent != null) StopCoroutine(shootEvent);
        }
    }
}