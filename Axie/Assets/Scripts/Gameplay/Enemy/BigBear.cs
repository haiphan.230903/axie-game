using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Gameplay.Player;
using DG.Tweening;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public class BigBear : Enemy
    {
        [Header("BigBear")]
        [SerializeField] float timeBetweenHit;
        [SerializeField] GameObject bearAttack;
        Coroutine bigBearJump, bigBearHit;
        void FixedUpdate()
        {
            if (!isCanMove && Hp > 0) 
            {
                anim.loop = true;
                anim.AnimationName = "action/idle/normal";
                return;
            }
            if (!isInAction)
                bigBearJump = StartCoroutine(Cor_bigBearJump());
        }
        IEnumerator Cor_bigBearJump()
        {
            isInAction = true;
            rb.constraints = RigidbodyConstraints.FreezeAll;
            Vector3 direction = Player.Instance.transform.position - transform.position;
            Flip(Player.Instance.transform.position.x);
            yield return new WaitForSeconds(0.75f);
            float timerForDropdown = 0f;
            anim.loop = false;
            anim.AnimationName = "action/move-back";

            while (timerForDropdown <= 1)
            {
                yield return new WaitForSeconds(Time.fixedDeltaTime);
                timerForDropdown += Time.fixedDeltaTime;
                float tempY = Mathf.Sin(timerForDropdown * Mathf.PI / 1) * 3;
                transform.position += direction / 1 * Time.fixedDeltaTime;
                transform.position = new Vector3(transform.position.x, tempY, transform.position.z);
            }
            transform.position = new(transform.position.x, 0, transform.position.z);

            yield return new WaitForSeconds(0.5f);
            rb.constraints = ~RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeAll;
            bigBearHit = StartCoroutine(AxeHit());
            yield return new WaitForSeconds(1);
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            yield return new WaitForSeconds(timeBetweenHit);
            isInAction = false;
        }
        IEnumerator AxeHit()
        {
            anim.loop = false;
            anim.AnimationName = "attack/melee/normal-attack";
            yield return new WaitForSeconds(0.8f);
            var pos = transform.position;
            pos.y = 0.1f;
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SHOT);
            var go = Instantiate(bearAttack, pos, Quaternion.Euler(90,0,0));
            Destroy(go, 0.5f);
        }
        [Button]
        public override void Die()
        {
            base.Die();
            if (bigBearJump != null) StopCoroutine(bigBearJump);
            if (bigBearHit != null) StopCoroutine(bigBearHit);
        }
        public override void GetHit(Vector3 attackPos, bool killTween = true)
        {
            base.GetHit(attackPos, false);
        }
    }
}