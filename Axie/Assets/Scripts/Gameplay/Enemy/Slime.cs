using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using GDC.Gameplay.Player;

namespace GDC.Enemies
{
    public class Slime : Enemy
    {
        protected override void Update()
        {
            base.Update();
            if (Hp > 0)
                Flip(Player.Instance.transform.position.x);
        }
        [Button]
        public override void Die()
        {
            base.Die();
            GetComponent<SlimeMove>().enabled = false;
        }
    }
}
