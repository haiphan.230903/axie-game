using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] MeshRenderer meshRenderer;
    [SerializeField] Collider coll;
    [SerializeField] float speed;
    public Vector3 dir;

    [SerializeField] GameObject vfx_explose;

    //[SerializeField] AudioSource audioSource;
    public void SetDir(float blasterAngle) //basterAngle in rad
    {
        //audioSource.Play();
        rb.velocity = Vector3.zero;
        dir = new Vector3(Mathf.Sin(blasterAngle), -0.2f, Mathf.Cos(blasterAngle)).normalized;
        transform.Translate(dir/2);
        dir.y = 0;
        StartCoroutine(Cor_Inactive(2));

        meshRenderer.enabled = true;
        coll.enabled = true;
    }
    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        transform.Translate(dir * speed * Time.deltaTime);
    }
    public void Explosion()
    {
        if (gameObject.activeSelf == false) return;
        vfx_explose.SetActive(true);
        meshRenderer.enabled = false;
        coll.enabled = false;
        StartCoroutine(Cor_Inactive(1));
    }
    private void OnCollisionEnter(Collision collision)
    {
        //audioSource.Play();
        Explosion();
        //Destroy(gameObject,1);
    }
    IEnumerator Cor_Inactive(float sec)
    {
        yield return new WaitForSeconds(sec);
        gameObject.SetActive(false);
        StopAllCoroutines();
    }

    private void OnDisable()
    {
        gameObject.SetActive(false);
    }
}
