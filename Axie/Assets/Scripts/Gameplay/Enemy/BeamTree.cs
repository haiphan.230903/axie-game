using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Gameplay.Player;
using DG.Tweening;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public class BeamTree : Enemy
    {
        [Header("BeamTree")]
        [SerializeField] GameObject beam;
        [SerializeField] Transform beamTip;
        [SerializeField] float speed;
        [SerializeField] float minX, maxX;
        Coroutine beamShoot;
        protected override void Update()
        {
            base.Update();
            if (Hp > 0)
                Flip(Player.Instance.transform.position.x);
        }
        void FixedUpdate()
        {
            if (!isCanMove) return;
            if (!isInAction)
                beamShoot = StartCoroutine(Cor_BeamShoot());
        }
        IEnumerator Cor_BeamShoot()
        {
            isInAction = true;
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            yield return new WaitForSeconds(1.5f);
            float delay = MoveToPlayerRow();
            yield return new WaitForSeconds(delay);
            anim.loop = false;
            anim.AnimationName = "attack/ranged/cast-multi";
            isCanFlip = false;
            yield return new WaitForSeconds(0.5f);
            Vector3 newDir = new(Player.Instance.transform.position.x - beamTip.position.x, 0, 0);
            ShootBeam(newDir);
            yield return new WaitForSeconds(0.5f);
            isCanFlip = true;
            isInAction = false;
        }
        /// <summary>
        /// Make BeamTree move to playerRow and returns the time of moving
        /// </summary>
        float MoveToPlayerRow()
        {
            var playerX = transform.position.x;
            Vector3 newPos;
            if (Mathf.Abs(playerX - minX) <= 1)
            {
                newPos = new(maxX - 2, 0, Player.Instance.transform.position.z);
            }
            else if (Mathf.Abs(playerX - maxX) <= 1)
            {
                newPos = new(minX + 2, 0, Player.Instance.transform.position.z);
            }
            else 
            {
                newPos = new(transform.position.x, 0, Player.Instance.transform.position.z);
            }
            float dist = (newPos - transform.position).magnitude;
            transform.DOMove(newPos, dist / speed);
            anim.loop = true;
            anim.AnimationName = "action/move-forward";
            return dist / speed;
        }
        void ShootBeam(Vector3 newDir)
        {
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SHOT);
            var go = Instantiate(beam, beamTip.position, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(newDir, false);
        }
        [Button]
        public override void Die()
        {
            base.Die();
            if (beamShoot != null) StopCoroutine(beamShoot);
        }
    }
}