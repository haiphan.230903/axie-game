using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Gameplay.Player;

namespace GDC.Enemies
{
    public class SlimeMove : MonoBehaviour
    {
        [SerializeField] Enemy enemy;
        [SerializeField] float moveTime, waitTime;
        float waitTimeHelper, moveTimeHelper, resetTime = 0f;
        Vector3 direction;
        float dist;
        [SerializeField] float minDistanceDetectPlayer, maxDistanceDetectPlayer;
        [HideInInspector] public bool slimeCanMove = true;
        bool canMoveNext, isMoving;
        void Start()
        {
            waitTimeHelper = waitTime;
            moveTimeHelper = 0f;
            canMoveNext = false;
        }
        void FixedUpdate()
        {
            if (!enemy.isCanMove)
            {
                ResetTime(0f, 0f, 0.2f);
                return;
            }
            direction = Player.Instance.transform.position - transform.position;
            dist = direction.magnitude;

            if (slimeCanMove && canMoveNext)
            {
                if (!isMoving)
                {
                    enemy.anim.loop = false;
                    enemy.anim.AnimationName = "action/move-forward";
                    // SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SLIME_WALK, 0.5f);
                    isMoving = true;
                }
                transform.position += Time.fixedDeltaTime * direction.normalized;
                moveTimeHelper -= Time.fixedDeltaTime;
                waitTimeHelper = waitTime;
            }
            else if (dist > maxDistanceDetectPlayer)
            {
                enemy.anim.AnimationName = "action/idle/normal";
            }
            if (resetTime >= 0f)
            {
                resetTime -= Time.fixedDeltaTime;
                return;
            }
            if (moveTimeHelper <= 0f)
            {
                slimeCanMove = false;
                isMoving = false;
                waitTimeHelper -= Time.fixedDeltaTime;

                if (dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer) canMoveNext = true;
                else canMoveNext = false;

                if (enemy.anim.AnimationName != "attack/ranged/cast")
                {
                    enemy.anim.loop = true;
                    enemy.anim.AnimationName = "action/idle/normal";
                }
            }
            if (waitTimeHelper <= 0f)
            {
                slimeCanMove = true;
                moveTimeHelper = moveTime;
            }
            if (!canMoveNext && dist <= maxDistanceDetectPlayer && dist >= minDistanceDetectPlayer)
            {
                canMoveNext = true;
            }
        }
        public void ResetTime(float resetTime, float moveTimeHelper, float waitTimeHelper)
        {
            this.resetTime = resetTime;
            this.moveTimeHelper = moveTimeHelper;
            this.waitTimeHelper = waitTimeHelper;
        }
    }
}