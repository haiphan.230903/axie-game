using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Gameplay.Player;
using DG.Tweening;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public class ThrowSlime : Enemy
    {
        [Header("ThrowSlime")]
        [SerializeField] Transform bulletTip;
        [SerializeField] List<GameObject> enemies;
        [SerializeField] float speed, timeBetweenHit;
        Coroutine shootEvent, moveEvent;
        void FixedUpdate()
        {
            if (!isCanMove) return;
            if (!isInAction)
                moveEvent = StartCoroutine(MoveToEnemy());
        }
        IEnumerator MoveToEnemy()
        {
            if (enemies.Count == 0) 
            {
                yield return null;
            }
            isInAction = true;
            var enemy = FindNearestEnemy();

            Flip(enemy.transform.position.x);
            while (Vector3.Distance(transform.position, enemy.transform.position) > 1.25f)
            {
                if (!isCanMove) 
                {
                    yield return new WaitForSeconds(Time.fixedDeltaTime);
                    continue;
                }
                anim.loop = true;
                anim.AnimationName = "action/move-forward";
                Vector3 dir = enemy.transform.position - transform.position;
                transform.position += dir.normalized * speed * Time.fixedDeltaTime;
                yield return new WaitForSeconds(Time.fixedDeltaTime);
            }
            shootEvent = StartCoroutine(Cor_ShootTheEnemy(enemy));
        }
        GameObject FindNearestEnemy()
        {
            float nearestDist = 10000f;
            GameObject result = null;
            foreach (var enemy in enemies)
            {
                float dist = Vector3.Distance(enemy.transform.position, transform.position);
                if (dist < nearestDist)
                {
                    nearestDist = dist;
                    result = enemy;
                }
            }
            return result;
        }
        IEnumerator Cor_ShootTheEnemy(GameObject enemy)
        {
            Flip(Player.Instance.transform.position.x);
            enemy.transform.position = bulletTip.position;
            var direction = Player.Instance.transform.position - enemy.transform.position;

            var enemyComponent = enemy.GetComponent<Enemy>();
            enemyComponent.anim.AnimationName = "action/idle/normal";
            enemyComponent.anim.timeScale = 0;
            enemyComponent.isCanFlip = false;
            enemyComponent.rb.constraints = RigidbodyConstraints.FreezeAll;

            anim.loop = false;
            anim.AnimationName = "attack/ranged/throw-slime";
            isCanFlip = false;
            yield return new WaitForSeconds(0.1f);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SHOT);

            Vector3 startPosition = enemy.transform.position;
            float timerForDropdown = 0f;
            Vector3 initialVelocity = new(direction.x, direction.y + 0.5f * 10, direction.z);

            while (timerForDropdown <= 1)
            {
                yield return new WaitForSeconds(Time.fixedDeltaTime);
                timerForDropdown += Time.fixedDeltaTime;
                Vector3 newPosition = startPosition + initialVelocity * timerForDropdown;
                newPosition.y = startPosition.y + (initialVelocity.y * timerForDropdown + 0.5f * -9.81f * timerForDropdown * timerForDropdown);
                enemy.transform.position = newPosition;
            }
            enemy.transform.position = new(enemy.transform.position.x, 0, enemy.transform.position.z);
            enemyComponent.anim.timeScale = 1;
            enemyComponent.isCanFlip = true;
            
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            yield return new WaitForSeconds(0.2f);
            isCanFlip = true;
            yield return new WaitForSeconds(timeBetweenHit);
            isInAction = false;
        }
        [Button]
        public override void Die()
        {
            base.Die();
            if (shootEvent != null) StopCoroutine(shootEvent);
            if (moveEvent != null) StopCoroutine(moveEvent);
        }
        public override void GetHit(Vector3 attackPos, bool killTween = true)
        {
            base.GetHit(attackPos, false);
        }
    }
}