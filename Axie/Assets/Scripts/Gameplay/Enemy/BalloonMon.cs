using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Gameplay.Player;
using DG.Tweening;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public enum BalloonMonArea {
        WEST, 
        EAST,
        NORTH
    }
    public class BalloonMon : Enemy
    {
        [Header("BalloonMon")]
        [SerializeField] BalloonMonArea currentArea;
        [SerializeField] GameObject balloonBullet;
        [SerializeField] Transform bulletTip;
        [SerializeField] float minX, maxX, minZ, maxZ;
        [SerializeField] float baseNorth, baseEast, baseWest;
        [SerializeField] float speed;
        Vector3 direction;
        int countShoot = 0;
        bool isShooting;
        float delay;
        Coroutine balloonShoot;
        void FixedUpdate()
        {
            if (!isCanMove) return;
            if (!isShooting)
                balloonShoot = StartCoroutine(Cor_BalloonMonShoot());
        }
        IEnumerator Cor_BalloonMonShoot()
        {
            isShooting = true;
            anim.loop = false;
            anim.AnimationName = "attack/ranged/cast-high";
            yield return new WaitForSeconds(0.5f);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SHOT);
            var go = Instantiate(balloonBullet, bulletTip.position, Quaternion.identity);
            go.GetComponent<EnemyBulletMove>().SetDirection(Player.Instance.transform.position - bulletTip.position, false);
            countShoot = (countShoot + 1) % 3;
            yield return new WaitForSeconds(0.2f);
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            yield return new WaitForSeconds(2);
            MoveToNewPos();
            yield return new WaitForSeconds(delay);
            Flip(Player.Instance.transform.position.x);
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            yield return new WaitForSeconds(0.5f);
            isShooting = false;
        }
        void MoveToNewPos()
        {
            if (countShoot == 0)
            {
                int rand = Random.Range(0,2);
                if (currentArea == BalloonMonArea.EAST)
                {
                    if (rand == 0) MoveTo(BalloonMonArea.WEST);
                    else MoveTo(BalloonMonArea.NORTH);
                }
                else if (currentArea == BalloonMonArea.WEST)
                {
                    if (rand == 0) MoveTo(BalloonMonArea.EAST);
                    else MoveTo(BalloonMonArea.NORTH);
                }
                else 
                {
                    if (rand == 0) MoveTo(BalloonMonArea.EAST);
                    else MoveTo(BalloonMonArea.WEST);
                }
            }
            else 
            {
                MoveTo(currentArea);
            }
        }
        void MoveTo(BalloonMonArea area)
        {
            currentArea = area;
            Vector3 newPos;
            if (area == BalloonMonArea.NORTH)
            {
                float newPosX = Random.Range(minX, maxX);
                newPos = new(newPosX, 0.45f, baseNorth);
            }
            else if (area == BalloonMonArea.WEST)
            {
                float newPosZ = Random.Range(minZ, maxZ);
                newPos = new(baseWest, 0.45f, newPosZ);
            }
            else
            {
                float newPosZ = Random.Range(minZ, maxZ);
                newPos = new(baseEast, 0.45f, newPosZ);
            }
            float dist = (newPos - transform.position).magnitude;
            delay = dist / speed;
            transform.DOMove(newPos, delay);
            anim.loop = true;
            anim.AnimationName = "action/move-forward";
        }
        public override void GetHit(Vector3 attackPos, bool killTween = true)
        {
            base.GetHit(attackPos, false);
        }
        [Button]
        public override void Die()
        {
            base.Die();
            if (balloonShoot != null) StopCoroutine(balloonShoot);
        }
    }
}