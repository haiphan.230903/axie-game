using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThinGroundController : Phase
{
    public List<ThinGround> thinGrounds;
    public List<int> moveType;
    bool isEnd;

    [Button]
    public override void Setup()
    {
        isEnd = false;
        for (int i = 0; i<thinGrounds.Count; i++)
        {
            if (moveType == null || moveType.Count == 0)
                thinGrounds[i].Grow();
            else
                thinGrounds[i].Grow(moveType[i]);
        }
        if (duration>0)
            StartCoroutine(Cor_End());
    }
    IEnumerator Cor_End()
    {
        yield return new WaitForSeconds(duration);
        HideAll();
        isEnd = true;
    }
    public override bool CheckEnd()
    {
        return isEnd;
    }
    public void HideAll()
    {
        foreach (var thinGround in thinGrounds)
        {
            if (thinGround.gameObject.activeSelf == true)
                thinGround.Hide();
        }
    }
}
