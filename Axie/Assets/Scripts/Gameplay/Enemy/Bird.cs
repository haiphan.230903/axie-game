using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Gameplay.Player;
using DG.Tweening;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public class Bird : Enemy
    {
        [Header("Bird")]
        [SerializeField] float speed;
        Vector3 direction;
        protected override void Start()
        {
            base.Start();
            direction = Player.Instance.transform.position - transform.position;
            direction.y = 0;
        }
        void FixedUpdate()
        {
            if (!isCanMove) return;
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            transform.position += direction.normalized * Time.fixedDeltaTime * speed;
        }
        public override void GetHit(Vector3 attackPos, bool killTween = true)
        {
            base.GetHit(attackPos, false);
        }
    }
}