using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using GDC.Gameplay.Player;

namespace GDC.Enemies
{
    public enum BulletMoveType 
    {
        STRAIGHT,
        DROPDOWN,
    }
    public class EnemyBulletMove : MonoBehaviour
    {
        [SerializeField] BulletMoveType bulletMoveType;
        [SerializeField] bool willDestroy;
        [SerializeField, HideIf("willDestroy")] bool willContinueUntilDone;
        public float speed;
        Vector3 startPosition;
        public float timer = 1f;
        float timerHelper;
        [Header("VFX")]
        [SerializeField] GameObject VFX_ExplodeWhenHitWall;
        [Header("STRAIGHT")]
        [SerializeField] Vector3 direction;
        [Header("DROPDOWN")]
        [SerializeField] float heightOfParabol;
        [SerializeField] float timeToDropDown;
        float timerForDropdown;
        [HideInInspector] public ParticleSystem thisVFX;
        Vector3 initialVelocity;
        
        void Awake()
        {
            startPosition = transform.position;
            thisVFX = GetComponentInChildren<ParticleSystem>();
        }
        void Start()
        {
            timerHelper = timer;
            timerForDropdown = 0f;
        }

        void FixedUpdate()
        {
            Move();
            timerHelper -= Time.fixedDeltaTime;
            if (timerHelper <= 0) 
            {
                timerHelper = timer;
                if (!willDestroy) gameObject.SetActive(false);
                else Destroy(gameObject);
            }
        }
        void Move()
        {
            switch (bulletMoveType)
            {
                case BulletMoveType.STRAIGHT:
                    transform.position += speed * Time.fixedDeltaTime * direction.normalized;
                    return;

                case BulletMoveType.DROPDOWN:
                    if (timerForDropdown > timeToDropDown) return;
                    timerForDropdown += Time.fixedDeltaTime;
                    
                    Vector3 newPosition = startPosition + initialVelocity * timerForDropdown;
                    newPosition.y = startPosition.y + (initialVelocity.y * timerForDropdown + 0.5f * -9.81f * timerForDropdown * timerForDropdown);
                    transform.position = newPosition;
                    return;
            }
        }
        public void SetDirection(Vector3 dir, bool setRotation = true)
        {
            initialVelocity = new Vector3(
                dir.x / timeToDropDown,
                (dir.y + 0.5f * heightOfParabol) / timeToDropDown,
                dir.z / timeToDropDown
            );

            direction = dir;
            if (setRotation)
            {
                transform.rotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg);
            }
        }
        public void SetDropDown(float heightOfParabol, float timeToDropDown, float speed)
        {
            this.heightOfParabol = heightOfParabol;
            this.timeToDropDown = timeToDropDown;
            this.speed = speed;
        }
        // public Vector3 GetDirection()
        // {
        //     return direction;
        // }
        // void OnTriggerStay2D(Collider2D collision)
        // {
        //     if (collision.CompareTag("Player"))
        //     {
        //         if (thisVFX != null) thisVFX.transform.SetParent(null);
        //         if (!willDestroy) 
        //         {
        //             if (!willContinueUntilDone) 
        //             {
        //                 gameObject.SetActive(false);
        //                 timerHelper = timer;
        //             }
        //         }
        //     }
        // }
        // void OnCollisionEnter2D(Collision2D collision)
        // {
        //     if (collision.gameObject.CompareTag("Wall"))
        //     {
        //         if (VFX_ExplodeWhenHitWall != null)
        //         {
        //             VFX_ExplodeWhenHitWall.transform.SetParent(null);
        //             VFX_ExplodeWhenHitWall.SetActive(true);
        //             if (!willDestroy) 
        //             {
        //                 if (!willContinueUntilDone) 
        //                 {
        //                     gameObject.SetActive(false);
        //                     timerHelper = timer;
        //                 }
        //             }
        //             else Destroy(gameObject);
        //         }
        //     }
        // }
        // void OnTriggerEnter2D(Collider2D collision)
        // {
        //     if (collision.CompareTag("Wall"))
        //     {
        //         if (VFX_ExplodeWhenHitWall != null)
        //         {
        //             VFX_ExplodeWhenHitWall.transform.SetParent(null);
        //             VFX_ExplodeWhenHitWall.SetActive(true);
        //             if (!willDestroy) 
        //             {
        //                 if (!willContinueUntilDone) 
        //                 {
        //                     gameObject.SetActive(false);
        //                     timerHelper = timer;
        //                 }
        //             }
        //             else Destroy(gameObject);
        //         }
        //     }
        // }
        // public void ResetVFXExplode()
        // {
        //     VFX_ExplodeWhenHitWall.SetActive(false);
        //     VFX_ExplodeWhenHitWall.transform.SetParent(transform);
        //     VFX_ExplodeWhenHitWall.transform.localPosition = Vector3.zero;
        // }
    }
}


