using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Managers;
using GDC.Gameplay.Player;
using DG.Tweening;
using NaughtyAttributes;

namespace GDC.Enemies
{
    public class MechaDog : Enemy
    {
        [Header("MechaDog")]
        [SerializeField] GameObject fireAttack;
        [SerializeField] GameObject waveAttack;
        [SerializeField] GameObject hitAttack;
        [SerializeField] Transform tip;
        [SerializeField] float timeBetweenHit;
        Coroutine fireHit, waveHit, hit;
        bool secondPhase, isMoving;
        protected override void Start()
        {
            base.Start();
            secondPhase = false;
        }
        void FixedUpdate()
        {
            if (!isCanMove) return;
            if (!isMoving)
            {
                int idx = Random.Range(0,3);
                if (idx == 0)
                {
                    fireHit = StartCoroutine(Cor_FireHit());
                }
                else if (idx == 1)
                {
                    waveHit = StartCoroutine(Cor_WaveHit());
                }
                else 
                {
                    hit = StartCoroutine(Cor_Hit());
                }
            }
        }
        IEnumerator Cor_FireHit()
        {
            isMoving = true;
            anim.loop = false;
            anim.AnimationName = "attack/ranged/cast-high";
            yield return new WaitForSeconds(1);

            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SHOT);

            var go = Instantiate(fireAttack, tip.position, Quaternion.identity);
            go.transform.localScale = Vector3.one;
            var direction = Player.Instance.transform.position - tip.position;
            var goMove = go.GetComponent<EnemyBulletMove>();
            goMove.SetDirection(direction, false);
            goMove.SetDropDown(15f, 1, 8);

            var go2 = Instantiate(fireAttack, tip.position, Quaternion.identity);
            go2.transform.localScale = Vector3.one;
            Vector3 offset2 = Random.insideUnitCircle * 4;
            offset2.z = offset2.y;
            offset2.y = 0;
            var direction2 = Player.Instance.transform.position + offset2 - tip.position;
            var goMove2 = go2.GetComponent<EnemyBulletMove>();
            goMove2.SetDirection(direction2, false);
            goMove2.SetDropDown(15f, 1, 8);

            yield return new WaitForSeconds(0.5f);
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            yield return new WaitForSeconds(timeBetweenHit);
            isMoving = false;
        }
        IEnumerator Cor_WaveHit()
        {
            isMoving = true;
            anim.loop = false;
            anim.AnimationName = "attack/melee/normal-attack";
            yield return new WaitForSeconds(0.75f);

            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SHOT);

            var go1 = Instantiate(waveAttack, tip.position, Quaternion.identity);
            go1.transform.localScale = Vector3.one;
            var direction1 = Player.Instance.transform.position - tip.position;
            var goMove1 = go1.GetComponent<EnemyBulletMove>();
            goMove1.SetDirection(direction1, false);
            goMove1.SetDropDown(15f, 1, 8);

            var go2 = Instantiate(waveAttack, tip.position, Quaternion.identity);
            go2.transform.localScale = Vector3.one;
            Vector3 offset2 = Random.insideUnitCircle * 4;
            offset2.z = offset2.y;
            offset2.y = 0;
            var direction2 = Player.Instance.transform.position + offset2 - tip.position;
            var goMove2 = go2.GetComponent<EnemyBulletMove>();
            goMove2.SetDirection(direction2, false);
            goMove2.SetDropDown(15f, 1, 8);

            var go3 = Instantiate(waveAttack, tip.position, Quaternion.identity);
            go3.transform.localScale = Vector3.one;
            Vector3 offset3 = Random.insideUnitCircle * 4;
            offset3.z = offset3.y;
            offset3.y = 0;
            var direction3 = Player.Instance.transform.position + offset3 - tip.position;
            var goMove3 = go3.GetComponent<EnemyBulletMove>();
            goMove3.SetDirection(direction3, false);
            goMove3.SetDropDown(15f, 1, 8);


            yield return new WaitForSeconds(0.5f);
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            yield return new WaitForSeconds(timeBetweenHit);
            isMoving = false;
        }
        IEnumerator Cor_Hit()
        {
            isMoving = true;
            anim.loop = false;
            anim.AnimationName = "action/random-01";
            yield return new WaitForSeconds(2.75f);
            SoundManager.Instance.PlaySoundRelatedToCamera(transform.position, AudioPlayer.SoundID.SFX_SHOT);
            var go = Instantiate(hitAttack, tip.position, Quaternion.identity);
            go.transform.localScale = Vector3.one * 0.75f;
            var direction = Player.Instance.transform.position - tip.position;
            direction.y = 0;
            var goMove = go.GetComponent<EnemyBulletMove>();
            goMove.SetDirection(direction, false);
            goMove.SetDropDown(15f, 1, 8);

            yield return new WaitForSeconds(1);
            anim.loop = true;
            anim.AnimationName = "action/idle/normal";
            yield return new WaitForSeconds(timeBetweenHit);
            isMoving = false;
        }
        [Button]
        public override void Die()
        {
            base.Die();
            if (fireHit != null) StopCoroutine(fireHit);
            if (waveHit != null) StopCoroutine(waveHit);
            if (hit != null) StopCoroutine(hit);
        }
        public override void GetHit(Vector3 attackPos, bool killTween = true)
        {
            base.GetHit(attackPos, false);

            if (Hp <= 10 && !secondPhase)
            {
                if (fireHit != null) StopCoroutine(fireHit);
                if (waveHit != null) StopCoroutine(waveHit);
                if (hit != null) StopCoroutine(hit);

                StartCoroutine(Cor_SecondPhase());
            }
        }
        IEnumerator Cor_SecondPhase()
        {
            secondPhase = true;
            anim.loop = false;
            anim.AnimationName = "battle/get-buff";
            float timer = 0f;
            while (timer <= 1.5f)
            {
                isCanMove = false;
                isMoving = true;
                isCanGetHit = false;
                yield return new WaitForSeconds(Time.fixedDeltaTime);
                timer += Time.fixedDeltaTime;
            }
            isCanGetHit = true;
            isCanMove = true;
            isMoving = false;
        }
    }
}