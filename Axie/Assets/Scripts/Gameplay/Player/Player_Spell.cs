using GDC.Gameplay.UI;
using GDC.PlayerManager.Spell;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Player
{
    public partial class Player
    {
        void CastSpell(SpellID spellID)
        {
            if (isDashing || isAttacking)
                return;
            if (spellDict[spellID].remainTime <= 0)
                return;
            if (isCasting)
                return;
            isCasting = true;
            spellDict[spellID].remainTime--;
            SpellDisplay.Instance.UpdateSpell(spellID, spellDict[spellID].remainTime);
            enableMove = false;
            SetAnimationState(AnimationState.CAST_SPELL);
            StartCoroutine(Cor_Cast(spellID));
            endCastCor = StartCoroutine(Cor_EndCast());
        }

        IEnumerator Cor_Cast(SpellID spellID)
        {
            Spell spell = Instantiate(spellDict[spellID].spell);
            spell.SetUp();
            yield return new WaitForSeconds(0.3f);
            spell.Cast();
            yield return new WaitForSeconds(0.3f);
            isCasting = false;
        }

        IEnumerator Cor_EndCast()
        {
            yield return new WaitForSeconds(0.6f);
            enableMove = true;
            SetAnimationState(AnimationState.IDLE_NORMAL);
            endCastCor = null;
        }

        void UseSpell() // use in update
        {
            if (Input.GetKey(KeyCode.U))
                CastSpell(SpellID.SPIKE);
            else if (Input.GetKey(KeyCode.I))
                CastSpell(SpellID.FIRE);
            else if (Input.GetKey(KeyCode.O))
                CastSpell(SpellID.BEAM);
        }
    }
}