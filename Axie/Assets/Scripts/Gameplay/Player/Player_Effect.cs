using DG.Tweening;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.RuleTile.TilingRuleOutput;

namespace GDC.Gameplay.Player
{
    public partial class Player
    {
        public void GetHit(Vector3 attackPos)
        {
            if (!isCanGetHit) 
                return;
            if (isDashing)
            {
                StopCoroutine(attackCor);
                isDashing = false;
            }
            if (dashCor!=null)
                StopCoroutine(dashCor);
            if (spawnDashCloneCor!=null)
                StopCoroutine(spawnDashCloneCor);
            if (endCastCor!=null)
                StopCoroutine(endCastCor);

            CameraEffects.Instance.ShakeOnce(0.5f, 8, Vector3.one, camera: Camera.main);
            isCanGetHit = false;
            enableMove = false;
            SetAnimationState(AnimationState.HIT);
            StartCoroutine(Cor_RefreshGetHit());
            Vector3 direct = (this.transform.position - attackPos);
            direct.y = 0;
            direct = direct.normalized;
            rb.velocity = direct*10;

            CurrentHP--;
            StartCoroutine(Cor_RefreshMove());
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HURT);
        }

        IEnumerator Cor_RefreshGetHit()
        {
            yield return new WaitForSeconds(timeToGetHitAgain);
            isCanGetHit = true;
        }

        IEnumerator Cor_RefreshMove()
        {
            graphicContainer.DOScale(0.8f, 0.15f);
            yield return new WaitForSeconds(0.15f);
            graphicContainer.DOScale(1, 0.15f);
            yield return new WaitForSeconds(0.35f);
            if (this.currentHp <= 0)
            {
                this.currentHp = 0;
                Die();
                yield break;
            }
            enableMove = true;
            SetAnimationState(AnimationState.IDLE_NORMAL);

        }
    }
}