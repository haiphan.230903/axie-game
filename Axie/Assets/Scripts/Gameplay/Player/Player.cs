using DG.Tweening;
using GDC.Enemies;
using GDC.Gameplay.UI;
using GDC.Managers;
using NaughtyAttributes;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Player
{
    public partial class Player : MonoBehaviour
    {
        private void Awake()
        {
            Instance = this;

            LoadPlayerStatus();
        }

        void LoadPlayerStatus()
        {
            currentHp = hp;
            StartCoroutine(Cor_LoadPlayerStatus());
        }

        IEnumerator Cor_LoadPlayerStatus()
        {
            yield return new WaitUntil(()=> SaveLoadManager.Instance!=null);
            int axieIdx = SaveLoadManager.Instance.GameData.currentAxieIndex;
            axies[axieIdx * 2].SetActive(true);
            meshRenderer[0] = axies[axieIdx * 2].GetComponent<MeshRenderer>();
            anim[0] = axies[axieIdx * 2].GetComponent<SkeletonAnimation>();
            axies[axieIdx * 2 + 1].SetActive(true);
            meshRenderer[1] = axies[axieIdx * 2 + 1].GetComponent<MeshRenderer>();
            anim[1] = axies[axieIdx * 2 + 1].GetComponent<SkeletonAnimation>();
        }


        // Update is called once per frame
        void Update()
        {
            if (isDie || isVictory || inCutScene)
                return;
            Move();
            Jump();
            Fall();
            Dash();
            NormalAttack();
            UseSpell();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.CompareTag("Finish"))
            {
                while (currentHp > 0)
                    CurrentHP--;
                Die();
            }

            if (isJumping && isAttacking && collision.transform.CompareTag("Ground"))
            {
                fallingCol.enabled = false;
                isJumping = false;
                isAttacking = false;
                enableMove = true;
                graphic.localRotation = Quaternion.Euler(0, 0, 0);
                Instantiate(jumpAttackDust, transform.position, Quaternion.identity).Play();
                StartCoroutine(Cor_EnableHitAfterJumpAttack());
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_LANDING);
                Attack();
            }

            if (collision.transform.CompareTag("Enemy"))
            {
                var enemy = collision.transform.GetComponent<Enemy>();
                if (enemy != null)
                {
                    if (enemy.Hp > 0)
                        GetHit(collision.transform.position);
                }
                else
                {
                    GetHit(collision.transform.position);
                }
            }
        }
        //private void OnCollisionStay(Collision collision)
        //{
        //    //if (collision.transform.CompareTag("Finish"))
        //    //{
        //    //    Die();
        //    //}    

        //    if (isJumping && isAttacking && collision.transform.CompareTag("Ground"))
        //    {
        //        fallingCol.enabled = false;
        //        isJumping = false;
        //        isAttacking = false;
        //        //isFalling = false;
        //        enableMove = true;
        //        graphic.localRotation = Quaternion.Euler(0, 0, 0);
        //        Instantiate(jumpAttackDust, transform.position, Quaternion.identity).Play();
        //        //jumpAttackDust.Play();
        //        Attack();
        //    }

        //    if (collision.transform.CompareTag("Enemy"))
        //    {
        //        var enemy = collision.transform.GetComponent<Enemy>();
        //        if (enemy != null)
        //        {
        //            if (enemy.Hp > 0)
        //                GetHit(collision.transform.position);
        //        }
        //        else
        //        {
        //            GetHit(collision.transform.position);
        //        }
        //    }

            
        //}

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.CompareTag("Ground") && !isAttacking && !isDie)
            {
                fallingCol.enabled = false;
                isJumping = false;
                Instantiate(jumpAttackDust, transform.position, Quaternion.identity).Play();
                SetAnimationState(AnimationState.IDLE_NORMAL);
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_LANDING);
                graphicContainer.DOScale(new Vector3(1.2f, 0.8f, 1), 0.2f).SetEase(Ease.InOutSine).OnComplete(() =>
                {
                    graphicContainer.DOScale(new Vector3(1, 1, 1), 0.2f).SetEase(Ease.InOutSine);
                });
            }

            if (other.transform.CompareTag("Enemy"))
            {
                var enemy = other.GetComponent<Enemy>();
                if (enemy != null)
                {
                    if (enemy.Hp > 0)
                    {
                        fallingCol.enabled = false;
                        isJumping = false;
                        GetHit(other.transform.position);
                    }
                }
                else
                {
                    fallingCol.enabled = false;
                    isJumping = false;
                    GetHit(other.transform.position);
                }
            }

        }
        [Button]
        void Die()
        {
            StopAllCoroutines();
            transform.position = Vector3.zero;
            isCanGetHit = false;
            rb.useGravity = false;
            rb.velocity = Vector3.zero;
            fallingCol.enabled = false;
            isDie = true;
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_DIE);
            GetComponent<BoxCollider>().isTrigger = true;
            StartCoroutine(Cor_Die());
        }

        IEnumerator Cor_Die()
        {
            Camera.main.transform.DOMoveZ(-8.4f, 1);
            SetAnimationState(AnimationState.SLEEP);
            for (int i = 0;i<4;i++)
            {
                graphic.DORotate(new Vector3(0, 0, -10), 0.2f);
                yield return new WaitForSeconds(0.2f);
                graphic.DORotate(new Vector3(0, 0, 10), 0.2f);
                yield return new WaitForSeconds(0.2f);
            }
            graphic.DOLocalMoveY(0.5f, testTimeDie/2).SetEase(Ease.Linear);
            SetAnimationState(AnimationState.DIE);
            yield return new WaitForSeconds(testTimeDie);
            anim[0].timeScale = 0;
            anim[1].timeScale = 0;
            EndGamePanel.Instance.GameOver();
            Time.timeScale = 0;
        
        }
        [Button]
        public void Victory()
        {
            StopAllCoroutines();
            transform.position = Vector3.zero;
            isCanGetHit = false;
            fallingCol.enabled = false;
            isVictory = true;
            GetComponent<BoxCollider>().isTrigger = true;
            StartCoroutine(Cor_Victory());
        }
        IEnumerator Cor_Victory()
        {
            Camera.main.transform.DOMoveZ(-8.4f, 1);
            SetAnimationState(AnimationState.VICTORY);
            yield return new WaitForSeconds(1.5f);
            anim[0].timeScale = 0;
            anim[1].timeScale = 0;
            EndGamePanel.Instance.Victory();
            Time.timeScale = 0;
        }

        void SetAnimationState(AnimationState state)
        {
            float timeScale = 1;
            if (state == AnimationState.MOVE)
                timeScale *= speed;
            else if (state == AnimationState.NORMAL_ATTACK)
                timeScale *= attackSpeed;
            if (anim[0]!=null && anim[1]!=null)
            {
                anim[0].timeScale = timeScale;
                anim[0].AnimationName = stateDict[state];
                anim[1].timeScale = timeScale;
                anim[1].AnimationName = stateDict[state];
            }
        }
    }
}