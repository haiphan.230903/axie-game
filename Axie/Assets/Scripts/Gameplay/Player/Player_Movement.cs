using DG.Tweening;
using DG.Tweening.Core.Easing;
using GDC.Managers;
using NaughtyAttributes;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Player
{
    public partial class Player
    {
        void Move()
        {
            if (!enableMove)
                return;

            var horizontalDirection = Input.GetAxisRaw("Horizontal");
            var verticalDirection = Input.GetAxisRaw("Vertical");

            if (horizontalDirection == 0 && verticalDirection == 0)
            {
                SetAnimationState(AnimationState.IDLE_NORMAL);
                rb.velocity = new Vector3(0,rb.velocity.y,0);
                return;
            }

            SetAnimationState(AnimationState.MOVE);
            if (horizontalDirection != 0 )
            {
                if (turnAroundTween != null)
                    turnAroundTween.Kill();
                turnAroundTween = graphic.DOScaleX(-horizontalDirection, 0.25f).Play(); ;
            }
            


            rb.velocity = new Vector3(0,rb.velocity.y,0) + new Vector3(horizontalDirection, 0, verticalDirection).normalized /** Time.deltaTime*/ * speed * speedScale;
            arrowAngle = Vector3.Angle(new Vector3(horizontalDirection, 0, verticalDirection), Vector3.right);
            if (verticalDirection < 0)
                arrowAngle *= -1;
            arrow.localPosition = new Vector3(arrowRange * Mathf.Cos(arrowAngle * Mathf.Deg2Rad), 0.3f, arrowRange * Mathf.Sin(arrowAngle * Mathf.Deg2Rad));
            arrow.rotation = Quaternion.Euler(90, 0, arrowAngle);
        }

        void Jump()
        {
            if (Input.GetKey(KeyCode.K))
            {
                if (isJumping)
                    return;
                if (isAttacking)
                    return;
                if (!isDashing)
                    enableMove = false;
                isJumping = true;
                graphicContainer.DOScale(new Vector3(1.2f, 0.8f, 1), 0.1f).SetEase(Ease.InOutSine).OnComplete(() =>
                {
                    rb.velocity = new Vector3(rb.velocity.x, jumpVelo, rb.velocity.z);
                    if (!isDashing)
                        enableMove = true;
                    graphicContainer.DOScale(new Vector3(1, 1, 1), 0.1f).SetEase(Ease.InOutSine).OnComplete(() =>
                    {
                        enableDashWhileJumping = true;
                        enableJumpAttack = true;
                    });
                });
                if (!isDashing)
                {
                    SetAnimationState(AnimationState.JUMP);
                }
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_JUMP);
            }
        }

        void Fall()
        {
            if (isJumping && rb.velocity.y < -1)
                fallingCol.enabled = true;
        }

        [Button]
        void GenerateDashClone()
        {
            Transform template = dashCloneContainer.GetChild(0);

            dashCloneList.Clear();
            while (dashCloneContainer.childCount > 1)
                DestroyImmediate(dashCloneContainer.GetChild(1).gameObject);

            dashCloneList.Add(template.transform);
            for (int i = 1; i < dashNum*2; i++)
            {
                Transform clone = Instantiate(template, dashCloneContainer);
                clone.name = "Circle";
                dashCloneList.Add(clone);
            }
        }

        IEnumerator Cor_SpawnCloneDash()
        {
            for (int i = 0;i < dashNum;i++)
            {
                Transform clone = dashCloneList[i];
                dashCloneList.RemoveAt(i);

                clone.position = transform.position + Vector3.up * 0.3f;
                clone.GetComponent<SpriteRenderer>().color = Color.white;
                clone.GetComponent<SpriteRenderer>().DOColor(Color.clear, 0.3f).SetDelay(0.2f);
                clone.localScale = Vector3.one * 0.75f ;
                clone.DOScale(0, 0.5f).OnComplete(()=> dashCloneList.Add(clone));
                yield return new WaitForSeconds(timeBetweenClone);
            }
        }

        void Dash()
        {
            if (Input.GetKeyDown(KeyCode.L) && !isDashing)
            {
                if (isAttacking)
                    return;
                if (isJumping && !enableDashWhileJumping)
                    return;
                if (isJumping)
                {
                    enableDashWhileJumping = false;
                }
                //rb.useGravity = false;
                isDashing = true;
                enableMove = false;
                SetAnimationState(AnimationState.NORMAL_ATTACK);
                var horizontalDirection = Input.GetAxisRaw("Horizontal");
                var verticalDirection = Input.GetAxisRaw("Vertical");
                attackCor = StartCoroutine(Cor_Attack());
                Vector3 dashDirection;
                if (horizontalDirection == 0 && verticalDirection == 0)
                {
                    dashDirection = new Vector3(dashVelo * Mathf.Cos(arrowAngle * Mathf.Deg2Rad), 0, dashVelo * Mathf.Sin(arrowAngle * Mathf.Deg2Rad));
                }
                else
                {
                    if (horizontalDirection != 0)
                    {
                        if (turnAroundTween != null)
                            turnAroundTween.Kill();
                        turnAroundTween = graphic.DOScaleX(-horizontalDirection, 0.25f).Play(); ;
                    }
                    Vector3 direction = new Vector3(horizontalDirection,0 , verticalDirection).normalized;
                    dashDirection = new Vector3(dashVelo * direction.x,0 ,dashVelo * direction.z);
                    arrowAngle = Vector3.Angle(dashDirection, Vector3.right);
                    if (verticalDirection < 0)
                        arrowAngle *= -1;
                    arrow.localPosition = new Vector3(arrowRange * Mathf.Cos(arrowAngle * Mathf.Deg2Rad), 0.3f, arrowRange * Mathf.Sin(arrowAngle * Mathf.Deg2Rad));
                    arrow.rotation = Quaternion.Euler(90, 0, arrowAngle);
                }

                rb.velocity = new Vector3(0, rb.velocity.y, 0);
                dashCor = StartCoroutine(Cor_Dash(dashDirection));
                spawnDashCloneCor = StartCoroutine(Cor_SpawnCloneDash());
                SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_DASH);
            }

            IEnumerator Cor_Dash(Vector3 dashDir)
            {
                float dashRemainTime = 0.3f;
                while (isDashing)
                {
                    transform.position += dashDir * 0.02f;
                    dashRemainTime -= 0.02f;
                    yield return new WaitForSeconds(0.02f);
                    if (dashRemainTime <= 0)
                    {
                        SetAnimationState(AnimationState.IDLE_NORMAL);
                        isDashing = false;
                        enableMove = true;
                        //if (isJumping)
                        //{
                        //    anim.timeScale = 1;
                        //    if (enableDashWhileJumping)
                        //        anim.AnimationName = stateDict[AnimationState.IDLE_NORMAL];
                        //}
                    }
                }
                dashCor = null;
            }
        }


    }
}