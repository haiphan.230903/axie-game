using DG.Tweening;
using GDC.Managers;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.Gameplay.Player
{
    public partial class Player
    {
        void NormalAttack()
        {
            if (Input.GetKey(KeyCode.J) && !isAttacking)
            {
                if (isDashing)
                    return;
                if (isJumping)
                {
                    JumpAttack();
                    return;
                }
                isAttacking = true;
                enableMove = false;
                SetAnimationState(AnimationState.NORMAL_ATTACK);
                StartCoroutine(Cor_Attack());
                StartCoroutine(Cor_EndAttack());
            }
        }

        IEnumerator Cor_Attack()
        {
            yield return new WaitForSeconds(delayUntilAttack/attackSpeed);
            Attack();
        }

        IEnumerator Cor_EndAttack()
        {
            yield return new WaitForSeconds(0.7f/attackSpeed);
            isAttacking = false;
            enableMove = true;
            SetAnimationState(AnimationState.IDLE_NORMAL);
        }

        void Attack()
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SLASH);
            Transform bullet = bullets[0];
            bullets.Remove(bullet);

            bullet.GetComponent<SpriteRenderer>().color = Color.white;
            bullet.localScale = Vector3.zero;
            bullet.gameObject.SetActive(true);
            bullet.DOScale(bulletScale, 0.25f);

            bullet.transform.position = graphic.position +
                new Vector3(attackRange.x * Mathf.Cos(arrowAngle * Mathf.Deg2Rad), 0.4f, attackRange.x * Mathf.Sin(arrowAngle * Mathf.Deg2Rad));
            bullet.transform.rotation = Quaternion.Euler(90, 0, arrowAngle);

            Tween movingTween = bullet.DOMove(graphic.position +
                new Vector3(attackRange.y * Mathf.Cos(arrowAngle * Mathf.Deg2Rad), 0.4f, attackRange.y * Mathf.Sin(arrowAngle * Mathf.Deg2Rad)), bulletFlyingDuration).SetEase(Ease.OutSine).OnComplete(() =>
                {
                    bullet.GetComponent<PlayerAttackObject>().Disappear();
                }).Play();
            bullet.GetComponent<SpriteRenderer>().DOFade(0, bulletFlyingDuration - 0.4f).SetDelay(0.4f).SetEase(Ease.Linear);
            bullet.GetComponent<Slash>().movingTween = movingTween;
            bullet.GetComponent<Slash>().SetUp();
        }

        void JumpAttack()
        {
            if (!enableJumpAttack) 
                return;
            float side = 0;
            var horizontalDirection = Input.GetAxisRaw("Horizontal");
            if (horizontalDirection > 0)
                side = 1;
            else if (horizontalDirection < 0)
                side = -1;
            else if (graphic.localScale.x < 0)
                side = 1;
            else
                side = -1;
                
            enableJumpAttack = false;
            isAttacking = true;
            enableMove = false;
            isCanGetHit = false;
            rb.velocity = new Vector3(rb.velocity.x + 4 * side, -jumpAttackVelo, rb.velocity.z);
            graphic.localRotation = Quaternion.Euler(0, 0, 45*-side);
            SetAnimationState(AnimationState.NORMAL_ATTACK);
        }

        IEnumerator Cor_EnableHitAfterJumpAttack()
        {
            yield return new WaitForSeconds(1);
            isCanGetHit = true;
        }    
    }
}
