using NaughtyAttributes;
using Spine.Unity;
using System.Collections.Generic;
using UnityEngine;
using RotaryHeart.Lib.SerializableDictionary;
using DG.Tweening;
using GDC.Gameplay.UI;
using GDC.PlayerManager.Spell;

namespace GDC.Gameplay.Player
{
    enum AnimationState
    {
        IDLE_NORMAL = 0,
        IDLE_1,
        IDLE_2,
        IDLE_3,
        IDLE_4,

        MOVE = 10,
        JUMP,
        //DASH,

        NORMAL_ATTACK = 20,
        CAST_SPELL,
        //DASH_ATTACK,
        //JUMP_ATTACK,

        BUFF = 30,
        DEBUFF,

        HIT = 40,
        DIE,
        SLEEP,
        VICTORY,
    }

    public enum SpellID
    {
        SPIKE = 0,
        BEAM = 10,
        FIRE = 20,
    }

    [System.Serializable]
    public class SpellInfo
    {
        public Spell spell;
        public int remainTime = 0;
    }

    [System.Serializable]
    class StateDict : SerializableDictionaryBase<AnimationState,string> { }

    [System.Serializable]
    public class SpellDict : SerializableDictionaryBase<SpellID, SpellInfo> { }

    public partial class Player
    {
        public static Player Instance { get; private set; }
        [SerializeField] float testTimeDie;
        [SerializeField] Rigidbody rb;
        [SerializeField] Transform graphicContainer;
        [SerializeField] Transform graphic;
        [SerializeField] MeshRenderer[] meshRenderer = new MeshRenderer[2];
        [SerializeField] SphereCollider fallingCol;
        bool isDie = false,isVictory = false;
        [SerializeField] bool isCanGetHit = true;
        [SerializeField] float timeToGetHitAgain;
        bool inCutScene = false;
        public bool InCutScene
        {
            get => inCutScene;
            set
            {
                if (value)
                {
                    StopAllCoroutines();
                    SetAnimationState(AnimationState.IDLE_NORMAL);
                }
                inCutScene = value;
            }

        }
        [SerializeField] List<GameObject> axies = new List<GameObject>();

        [SerializeField,Foldout("Animation")] SkeletonAnimation[] anim = new SkeletonAnimation[2];
        [SerializeField, Foldout("Animation")] StateDict stateDict;

        [SerializeField, Foldout("Stat")] int hp;
        [SerializeField, Foldout("Stat"), ReadOnly] int currentHp;
        public int CurrentHP
        {
            get => currentHp;
            set
            {
                if (value > hp)
                    return;
                if (value < currentHp)
                    Heart.Instance.Hurt(value);
                else if (value > currentHp)
                    Heart.Instance.Heal(currentHp);
                currentHp = value;
            }
        }

        [SerializeField, Foldout("Stat")] float speed;
        [SerializeField, Foldout("Stat")] float attackSpeed = 1;
        public float AttackSpeed
        {
            get => attackSpeed;
            set => attackSpeed = value;
        }    
            
        public float Speed
        {
            get => speed;
            set => speed = value;
        }
        [SerializeField, Foldout("Stat")] float speedScale;

        [SerializeField, Foldout("Movement")] float jumpVelo;
        [SerializeField, Foldout("Movement")] float dashVelo;
        [SerializeField]bool isJumping = false,isDashing = false;
        [SerializeField, Foldout("Movement")] List<Transform> dashCloneList = new List<Transform>();
        [SerializeField, Foldout("Movement")] int dashNum;
        [SerializeField, Foldout("Movement")] float timeBetweenClone;
        [SerializeField, Foldout("Movement")] Transform dashCloneContainer;

        Coroutine dashCor,spawnDashCloneCor;
        Tween turnAroundTween;
        bool enableDashWhileJumping = false;
        bool enableJumpAttack = false;
        [SerializeField, Foldout("Movement")] ParticleSystem jumpAttackDust;
        bool enableMove = true;
        public bool EnableMove
        {
            get => enableMove;
            set => enableMove = value;
        }
        [SerializeField, Foldout("Movement")] Transform arrow;
        public float ArrowAngle => arrowAngle;
        float arrowAngle = 180; // axie initially face the left
        [SerializeField, Foldout("Movement")] float arrowRange;

        [SerializeField, Foldout("Attack")] List<Transform> bullets = new List<Transform>();
        public List<Transform> Bullets
        {
            get => bullets;
            set => bullets = value;
        }
        [SerializeField, Foldout("Attack")] float bulletScale = 0.5f;
        public float BulletScale
        {
            get => bulletScale; 
            set => bulletScale = value;
        }    
        bool isAttacking = false;
        Coroutine attackCor;
        [SerializeField, Foldout("Attack")] Vector2 attackRange;
        [SerializeField, Foldout("Attack")] float delayUntilAttack;
        [SerializeField, Foldout("Attack")] float bulletFlyingDuration;
        [SerializeField, Foldout("Attack")] float jumpAttackVelo;

        [SerializeField, Foldout("Spell")] SpellDict spellDict;
        public SpellDict SpellDict
        {
            get => spellDict;   
        }
        Coroutine endCastCor;
        [SerializeField] bool isCasting = false;
    }
}