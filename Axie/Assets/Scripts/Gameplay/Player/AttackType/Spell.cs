using DG.Tweening;
using GDC.Gameplay.UI;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GDC.PlayerManager.Spell
{
    public abstract class Spell : MonoBehaviour
    {
        //[HideInInspector] public bool IsCasting = false;

        public virtual void SetUp() { }

        public abstract void Cast();
        public virtual void EndSpell() { }

    }
}
