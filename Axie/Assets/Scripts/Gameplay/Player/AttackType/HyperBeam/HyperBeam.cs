using DG.Tweening;
using GDC.Gameplay;
using GDC.Gameplay.Player;
using GDC.Managers;
using GDC.PlayerManager;
using GDC.PlayerManager.Spell;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HyperBeam : Spell
{

    [SerializeField, Foldout("Beam")] Transform[] beams;
    Vector3 spellPos;
    [SerializeField, Foldout("Hyper Beam Info")] float beamDuration;
    [SerializeField, Foldout("Hyper Beam Info")] Transform beamContainer;
    [SerializeField, Foldout("Hyper Beam Info")] GameObject colliderContainer;

    public override void SetUp()
    {
        spellPos = Player.Instance.transform.position + Vector3.up * 2;
        Player.Instance.transform.DOMoveY(transform.position.y, 0.3f).SetEase(Ease.Linear);
    }
    public override void Cast()
    {
        transform.position = spellPos;
        float arrowAngle = Player.Instance.ArrowAngle;
        chargingAnimation.gameObject.SetActive(true);
        transform.localRotation = Quaternion.Euler(new Vector3(0, -arrowAngle,0));
        
        chargingAnimation.DOScale(0, 0.5f).OnComplete(() =>
        {
            //SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HYPER_BEAM);
            CameraEffects.Instance.ShakeOnce(beamDuration, 10, camera: Camera.main);
            colliderContainer.SetActive(true);
            beamContainer.gameObject.SetActive(true);
            StartCoroutine(Cor_BeamAnimation());
            StartCoroutine(Cor_BeamScale());
        });
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_HYPER_BEAM);
    }

    [SerializeField, Foldout("Beam Animation")] Sprite[] beamSprites;
    [SerializeField, Foldout("Beam Animation")] Sprite[] beamTrailSprites;
    [SerializeField, Foldout("Beam Animation")] List<SpriteRenderer> beamSpriteRenderers = new List<SpriteRenderer>();
    [SerializeField, Foldout("Beam Animation")] List<SpriteRenderer> beamTrailSpriteRenderers = new List<SpriteRenderer>();
    [SerializeField, Foldout("Beam Animation")] int loopTime;

    IEnumerator Cor_BeamScale()
    {
        yield return new WaitForSeconds(beamDuration*0.8f);
        foreach (Transform beam in beams)
        {
            beam.DOScaleY(0, beamDuration*0.2f);
        }
    }

    IEnumerator Cor_BeamAnimation()
    {
        int numberOfSprite = beamSprites.Length;

        for (int loop = 0; loop < loopTime; loop++)
        {
            for (int i = 0; i < numberOfSprite; i++)
            {
                foreach (SpriteRenderer beamSpriteRenderer in beamSpriteRenderers)
                    beamSpriteRenderer.sprite = beamSprites[i];
                foreach (SpriteRenderer beamTrailSpriteRenderer in beamTrailSpriteRenderers)
                    beamTrailSpriteRenderer.sprite = beamTrailSprites[i];
                yield return new WaitForSeconds(beamDuration / (numberOfSprite * loopTime));
            }
        }

        Destroy(gameObject);
    }
    [SerializeField, Foldout("Charge Animation")] Transform chargingAnimation;
}
