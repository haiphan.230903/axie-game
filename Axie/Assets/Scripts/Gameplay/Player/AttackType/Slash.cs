using DG.Tweening;
using GDC.Gameplay.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slash : PlayerAttackObject
{
    [HideInInspector] public Tween movingTween;
    [SerializeField] TrailRenderer trail;
    [SerializeField] ParticleSystem template;

    public override void SetUp()
    {
        trail.enabled = true;
    }

    public override void Disappear()
    {
        if (movingTween != null)
            movingTween.Kill();
        trail.enabled = false;
        gameObject.SetActive(false);

        Player.Instance.Bullets.Add(this.transform);
        ParticleSystem clone = Instantiate(template, transform.position, Quaternion.identity);
        clone.Play();
    }
}
