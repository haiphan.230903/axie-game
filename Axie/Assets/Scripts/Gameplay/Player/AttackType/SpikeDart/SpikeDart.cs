using DG.Tweening;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GDC.Gameplay.UI;
using GDC.Managers;
using GDC.Gameplay.Player;

namespace GDC.PlayerManager.Spell
{
    public class SpikeDart : Spell
    {
        [SerializeField] Vector2 range;
        [SerializeField] float spikeDuration;
        [SerializeField] int numberOfSpike;

        [SerializeField] Transform spikeContainer;
        [SerializeField] List<Transform> spikes = new List<Transform>();

        [Button]
        void GenerateSpike()
        {
            if (numberOfSpike == 0)
                return;

            spikes.Clear();

            Transform template = Instantiate(spikeContainer.GetChild(0));
            while (spikeContainer.childCount > 0)
                DestroyImmediate(spikeContainer.GetChild(0).gameObject);

            for (int i = 0; i < numberOfSpike; i++)
            {
                Transform clone = Instantiate(template, spikeContainer);
                clone.name = "Spike";
                clone.localPosition = new Vector3
                    (range.x * Mathf.Cos(360 / numberOfSpike * i * Mathf.Deg2Rad), 0,
                    range.x * Mathf.Sin(360 / numberOfSpike * i * Mathf.Deg2Rad));
                clone.transform.localRotation = Quaternion.Euler(new Vector3(0, -360 / numberOfSpike *i,0));
                spikes.Add(clone);
            }

            DestroyImmediate(template.gameObject);
        }

        public override void Cast()
        {
            transform.position = Player.Instance.transform.position + Vector3.up * 0.4f;
            for (int i = 0; i < numberOfSpike; i++)
            {
                Transform spike = spikes[i];
                spike.DOScale(1, 0.25f);
                spike.DOLocalMove(
                    new Vector3(range.y * Mathf.Cos(360 / numberOfSpike * i * Mathf.Deg2Rad), 0,
                    range.y * Mathf.Sin(360 / numberOfSpike * i * Mathf.Deg2Rad))
                    , spikeDuration);
            }
            StartCoroutine(Cor_EndSpell());
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_SPIKE);
        }

        IEnumerator Cor_EndSpell()
        {
            yield return new WaitForSeconds(spikeDuration);
            Destroy(gameObject);
        }
    }
}