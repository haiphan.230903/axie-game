using DG.Tweening;
using GDC.Gameplay.Player;
using GDC.Managers;
using GDC.PlayerManager.Spell;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRing : Spell
{
    [SerializeField] List<Transform> fires = new List<Transform>();
    [SerializeField] float range,duration;


    public override void Cast()
    {
        for (int i = 0; i < fires.Count; i++)
        {
            Transform fire = fires[i];
            float currentAngle = 360 * i / fires.Count;
            fire.localPosition = new Vector3(range * Mathf.Cos(currentAngle*Mathf.Deg2Rad), 0, range * Mathf.Sin(currentAngle * Mathf.Deg2Rad));
            fire.DOScale(1, 0.5f).SetEase(Ease.OutBack);
            DOTween.To(() => currentAngle, value => currentAngle = value, currentAngle + 360, 2)
                .SetEase(Ease.Linear)
                .SetLoops(-1)
                .OnUpdate(()=> fire.localPosition = new Vector3(range * Mathf.Cos(currentAngle * Mathf.Deg2Rad), 0, range * Mathf.Sin(currentAngle * Mathf.Deg2Rad)));
        }
        StartCoroutine(Cor_EndSpell());
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_FIRE);
    }

    IEnumerator Cor_EndSpell()
    {
        yield return new WaitForSeconds(duration);
        foreach (Transform fire in fires)
            fire.DOScale(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }

    private void Update()
    {
        transform.position = Player.Instance.transform.position + Vector3.up * 0.2f;
    }
}
