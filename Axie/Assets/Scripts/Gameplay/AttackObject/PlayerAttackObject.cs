using DG.Tweening;
using GDC.Enemies;
using GDC.Gameplay.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackObject : MonoBehaviour
{
    [SerializeField] bool disappearWhenHit = false;

    public virtual void SetUp()
    {

    }


    public virtual void Disappear()
    {
        Destroy(gameObject);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            other.GetComponent<Enemy>()?.GetHit(transform.position);
            if (disappearWhenHit) 
                Disappear();
        }
    }
}
