using DG.Tweening;
using GDC.Gameplay.Player;
using GDC.Gameplay.UI;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeSpot : Phase
{
    [SerializeField] ParticleSystem vfx;
    [SerializeField] Collider coll;
    [SerializeField] Light light;
    bool isDone;
    [Button]
    public void Show()
    {
        isDone = false;
        coll.enabled = false;
        gameObject.SetActive(true);
        vfx.loop = true;
        light.intensity = 0;
        light.DOIntensity(0.5f, 0.6f);
        transform.DOScale(1, 0.6f).SetEase(Ease.OutBack);
        StartCoroutine(Cor_EnableColl());
    }
    IEnumerator Cor_EnableColl()
    {
        yield return new WaitForSeconds(1.5f);
        coll.enabled = true;
    }    
    [Button]
    public void Hide()
    {
        coll.enabled = false;
        vfx.loop = false;
        light.DOIntensity(0f, 0.5f);
        transform.DOScale(0, 0.6f).OnComplete(() =>
        {
            isDone = true;
            gameObject.SetActive(false);
        });
    }
    public override void Setup()
    {
        Show();
    }
    public override bool CheckEnd()
    {
        return isDone;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Player.Instance.CurrentHP > 0)
            {
                RewardPanel.Instance.Show();
                StartCoroutine(Cor_EndRewardPanel());
            }
        }
    }
    IEnumerator Cor_EndRewardPanel()
    {
        yield return new WaitUntil(() => RewardPanel.Instance.isShowing == false);
        Hide();
    }    
}