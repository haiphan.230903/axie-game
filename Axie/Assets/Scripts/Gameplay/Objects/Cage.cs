using DG.Tweening;
using GDC.Gameplay.Player;
using GDC.Gameplay.UI;
using GDC.Managers;
using NaughtyAttributes;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cage : Phase
{
    [SerializeField] Transform vfx_light, vfx_smoke, cageGraphic;
    [SerializeField] Collider coll;
    [SerializeField] List<SkeletonAnimation> axies;
    [SerializeField]
    [SpineAnimation]
    private string happyAnimName;

    override public void Setup()
    {
        Show();
    }
    public override bool CheckEnd()
    {
        return false;
    }
    [Button]
    public void Show()
    {
        coll.enabled = false;
        gameObject.SetActive(true);
        transform.localScale = Vector3.zero;
        transform.DOScale(1, 0.5f).SetEase(Ease.OutBack);
        StartCoroutine(Cor_EnableColl());
    }
    IEnumerator Cor_EnableColl()
    {
        yield return new WaitForSeconds(1);
        coll.enabled = true;
    }    
    [Button]
    public void Break()
    {
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_LIGHT_TRANSITION);
        CameraController.Instance.isFollowPlayer = false;
        Player.Instance.InCutScene = true;
        Camera.main.transform.DOMoveZ(-7.5f, 1);
        Camera.main.DOFieldOfView(30, 1);
        vfx_light.gameObject.SetActive(true);
        transform.DOMoveY(transform.position.y + 2, 2).SetEase(Ease.Linear).OnComplete(()=>
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ENEMY_APPEAR);
            StartCoroutine(Cor_VfxBreak());
        });
    }
    IEnumerator Cor_VfxBreak()
    {
        yield return new WaitForSeconds(2);
        Camera.main.DOFieldOfView(40, 1);
        CameraController.Instance.isFollowPlayer = true;
        cageGraphic.gameObject.SetActive(false);
        vfx_light.gameObject.SetActive(false);
        vfx_smoke.gameObject.SetActive(true);
        vfx_smoke.SetParent(null);
        foreach (var axie in axies)
        {
            axie.transform.SetParent(null);
            axie.transform.DOMoveY(0, 0.6f).SetEase(Ease.Linear).OnComplete(() =>
            {
                axie.transform.DOScaleX(1.2f, 0.25f).SetLoops(2, LoopType.Yoyo);
                axie.transform.DOScaleX(0.75f, 0.25f).SetLoops(2, LoopType.Yoyo).OnComplete(() =>
                {
                    axie.AnimationName = happyAnimName;
                });
            });
        }
        yield return new WaitForSeconds(3);
        EndGamePanel.Instance.Victory(); 
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Player.Instance.CurrentHP>0)
                Break();
        }
    }
}
