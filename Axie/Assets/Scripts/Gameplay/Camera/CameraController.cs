using DG.Tweening;
using GDC.Gameplay.Player;
using NaughtyAttributes;
using System.Collections;
//using System.Collections.Generic;
//using UnityEditor.Networking.PlayerConnection;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance { get; private set; }
    public float maxX = 0, minX = 0, maxY = 0, minY = 0, maxZ = 0, minZ = 0;
    public float moveSpeed;

    public bool isFollowPlayer, isOn2D;
    //bool isScoll = false;

    Vector3 target;
    [SerializeField] Tutorial tutorial;
    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        //anim = GetComponent<Animator>();
        //rigi = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        isFollowPlayer = false;
        isOn2D = true;
        transform.position = new Vector3(0, 0, -18);
        transform.rotation = Quaternion.Euler(0, 0, 0);
        StartCoroutine(Cor_ToGameplay());
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.Instance != null && isFollowPlayer)
        {
            // Tao camera di chuyen theo targetObj
            target = Player.Instance.transform.position;
            target.z -= 12;

            if (target.x < minX) target.x = minX;
            if (target.x >= maxX) target.x = maxX;
            if (target.y < minY) target.y = minY;
            if (target.y >= maxY) target.y = maxY;
            if (target.z < minZ) target.z = minZ;
            if (target.z >= maxZ) target.z = maxZ;            
            
            transform.position = Vector3.Lerp(transform.position, target, moveSpeed);
        }

        //if (isFollowPlayer == false) Invoke("FollowPlayer", 0.5f);
    }

    public Vector2 CameraPos
    {
        get { return transform.position; }
        set { 
                transform.position = value;
            transform.position -= new Vector3(0, 0, 10);
            }
    }
    IEnumerator Cor_ToGameplay()
    {
        yield return new WaitUntil(() => Player.Instance != null);
        Player.Instance.InCutScene = true;
        yield return new WaitForSeconds(2);
        ToGameplay();
    }
    [Button]
    public void ToGameplay()
    {
        isOn2D = false;
        transform.DORotate(new Vector3(35, 0, 0),2);
        //transform.DOMoveX(0, 2);
        transform.DOMoveY(7, 2);
        transform.DOMoveZ(-12, 2);
        StartCoroutine(Cor_FollowPlayer(2.2f));
    }
    [Button]
    public void To2D()
    {
        isFollowPlayer = false;
        isOn2D = true;
        transform.DORotate(new Vector3(0, 0, 0), 2);
        transform.DOMoveX(0, 2);
        transform.DOMoveY(0, 2);
        transform.DOMoveZ(-18, 2);
    }
    IEnumerator Cor_FollowPlayer(float sec)
    {
        yield return new WaitForSeconds(sec);
        if (isOn2D == false)
        {
            isFollowPlayer = true;
            Player.Instance.InCutScene = false;
        }
        LevelController.Instance?.SetUpPhase();
        tutorial?.Show();
    }
}
