using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAppearance : MonoBehaviour
{
    public GameObject enemy;
    public bool isDoneStarting;
    // Start is called before the first frame update
    void Start()
    {
        isDoneStarting = false;
        StartCoroutine(Cor_Appear());
    }
    IEnumerator Cor_Appear()
    {
        yield return new WaitForSeconds(2.5f);
        enemy.SetActive(true);
        isDoneStarting = true;
    }    
}
