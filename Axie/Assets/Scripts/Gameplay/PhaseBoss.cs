using DG.Tweening;
using GDC.Enemies;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseBoss : Phase
{
    [SerializeField] EnemyAppearance bossAppearance;
    [SerializeField] Enemy boss;
    [SerializeField] List<EnemyAppearance> enemyAppearances;
    [SerializeField] ThinGroundController thinGroundController;
    [SerializeField] int bossHpEndPhase;
    [SerializeField] bool isFirstBossAppear;
    public override void Setup()
    {
        CameraController.Instance.maxX = 5;
        if (isFirstBossAppear)
        {
            SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ENEMY_PREPARE_APPEAR);
            StartCoroutine(Cor_Appear());
            bossAppearance.gameObject.SetActive(true);
        }

        if (enemyAppearances.Count != 0)
        {
            foreach (var enemyAppear in enemyAppearances)
            {
                enemyAppear.gameObject.SetActive(true);
            }
        }
        if (thinGroundController != null)
        {
            thinGroundController.Setup();
        }
    }
    IEnumerator Cor_Appear()
    {
        yield return new WaitForSeconds(2);
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ENEMY_APPEAR);
    }
    public override bool CheckEnd()
    {
        bool kt = (int)boss.Hp <= bossHpEndPhase;
        if (kt)
        {
            if (enemyAppearances.Count != 0)
            {
                foreach (var enemyAppear in enemyAppearances)
                {
                    enemyAppear.transform.DOScale(0, 0.5f).OnComplete(() => enemyAppear.gameObject.SetActive(false));
                }
            }
            if (thinGroundController != null)
            {
                thinGroundController.HideAll();
            }
        }
        return kt;
    }
}
