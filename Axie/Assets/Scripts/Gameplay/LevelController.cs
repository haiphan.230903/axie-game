using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.RendererUtils;

public class LevelController : MonoBehaviour
{
    public static LevelController Instance { get; private set; }
    [SerializeField] List<Phase> phases;
    int i = 0;

    private void Awake()
    {
        Instance = this;
    }
    [Button()]
    public void SetUpPhase()
    {
        if (i>=phases.Count)
        {
            Debug.Log("End level");
            return;
        }
        Debug.Log("Start phase " + i);
        phases[i].Setup();
        StartCoroutine(Cor_EndPhase());
    }
    IEnumerator Cor_EndPhase()
    {
        if (phases[i].duration <= 0)
        {
            yield return new WaitUntil(() => phases[i].CheckEnd());
        }
        else
        {
            yield return new WaitForSeconds(phases[i].duration);
        }    
        i++;
        SetUpPhase();
    }
}
