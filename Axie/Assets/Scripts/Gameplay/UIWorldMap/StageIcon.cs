using DG.Tweening;
using GDC.Constants;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageIcon : MonoBehaviour
{
    [SerializeField] Image icon, maskIcon;
    [SerializeField] Image borderImage;
    [SerializeField] Image lockImage;
    
    [SerializeField] Image[] stars;
    [SerializeField] Sprite yellowStar;

    public RectTransform rectTransform;
    public int levelIndex;
    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }
    public void Setup(bool isLock)
    {
        if (isLock)
        {
            lockImage.gameObject.SetActive(true);
            icon.color = Color.gray;
        }
        else
        {
            lockImage.gameObject.SetActive(false);
            icon.color = Color.white;
        }

        SetStar();
    }
    void SetStar()
    {
        for (int i=0; i < SaveLoadManager.Instance.GameData.levelStars[levelIndex]; i++)
        {
            stars[i].sprite = yellowStar;
        }
    }    
    public void Show(bool isTween = true)
    {
        if (isTween)
        {
            icon.DOFade(1, 0.5f);
            maskIcon.DOFade(1, 0.5f);
            borderImage.DOFade(1, 0.5f);
            transform.SetSiblingIndex(GameConstants.MAX_LEVEL - 1);
            rectTransform.DOScale(1f, 0.5f);
            rectTransform.DOAnchorPosX(0, 0.5f);
        }
        else
        {
            Color currentIconColor = icon.color;
            currentIconColor.a = 1;
            icon.color = currentIconColor;
            maskIcon.color = currentIconColor;
            borderImage.color = Color.black;
            transform.SetSiblingIndex(GameConstants.MAX_LEVEL - 1);
            rectTransform.localScale = Vector3.one;
            rectTransform.anchoredPosition = Vector2.zero;
        }
    }
    public void Hide(bool isTween = true, bool isLeft = true)
    {
        if (isTween)
        {
            icon.DOFade(0.5f, 0.5f);
            maskIcon.DOFade(0.5f, 0.5f);
            borderImage.DOFade(0.5f, 0.5f);
            rectTransform.DOScale(0.8f, 0.5f);
            if (isLeft)
            {
                rectTransform.DOAnchorPosX(-180, 0.5f);
            }
            else
            {
                rectTransform.DOAnchorPosX(180, 0.5f);
            }
        }
        else
        {
            Color currentIconColor = icon.color;
            currentIconColor.a = 0.5f;
            icon.color = currentIconColor;
            maskIcon.color = currentIconColor;
            Color blackFade = Color.black;
            blackFade.a = 0.5f;
            borderImage.color = blackFade;
            rectTransform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            if (isLeft)
            {
                rectTransform.anchoredPosition = new Vector2(-180, 0);
            }
            else
            {
                rectTransform.anchoredPosition = new Vector2(180, 0);
            }
        }
    }    
}
