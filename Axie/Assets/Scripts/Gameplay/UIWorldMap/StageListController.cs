using DG.Tweening;
using GDC.Constants;
using GDC.Gameplay.UI;
using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class StageListController : MonoBehaviour
{
    [SerializeField] StageIcon[] stageIcons;
    [SerializeField] TMP_Text[] stageNames;
    [SerializeField] Color[] stageColors;
    [SerializeField] Button prevLevelBtn, nextLevelBtn;
    [SerializeField] CustomButton goButton;
    [SerializeField] Camera cam;
    int currentLevelIndex;
    private void Start()
    {
        StartCoroutine(Cor_Load());
    }
    IEnumerator Cor_Load()
    {
        yield return new WaitUntil(() => SaveLoadManager.Instance.isLoadingData);
        Setup();
    }
    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Alpha0))
    //    {
    //        SaveLoadManager.Instance.GameData.levelIndex = 3;
    //        Setup();
    //    }
    //}
    public void Setup()
    {
        currentLevelIndex = SaveLoadManager.Instance.GameData.currentLevelIndex;
        foreach(var stageName in stageNames)
        {
            stageName.text = "STAGE " + (currentLevelIndex + 1).ToString();
        }
        cam.backgroundColor = stageColors[currentLevelIndex];

        for(int i = 0; i<GameConstants.MAX_LEVEL; i++)
        {
            if (i<=SaveLoadManager.Instance.GameData.levelIndex)
            {
                stageIcons[i].Setup(false);
            }
            else
            {
                stageIcons[i].Setup(true);
            }

            if (i < currentLevelIndex)
            {
                stageIcons[i].Hide(false, true);
            }
            else if (i > currentLevelIndex)
            {
                stageIcons[i].Hide(false, false);
            }
            else
            {
                stageIcons[i].Show(false);
            }
        }
        CheckLevelBtn();
    }    
    void CheckLevelBtn()
    {
        if (currentLevelIndex == 0) prevLevelBtn.interactable = false;
        else prevLevelBtn.interactable = true;
        if (currentLevelIndex == GameConstants.MAX_LEVEL - 1) nextLevelBtn.interactable = false;
        else nextLevelBtn.interactable = true;

        SaveLoadManager.Instance.GameData.currentLevelIndex = currentLevelIndex;
        if (currentLevelIndex > SaveLoadManager.Instance.GameData.levelIndex) goButton.isDisable = true;
        else goButton.isDisable = false;
    }
    public void NextLevel()
    {
        stageIcons[currentLevelIndex].Hide(true, true);
        currentLevelIndex++;
        stageIcons[currentLevelIndex].Show();
        CheckLevelBtn();
        Camera.main.DOColor(stageColors[currentLevelIndex],1);
        foreach (var stageName in stageNames)
        {
            stageName.text = "STAGE " + (currentLevelIndex + 1).ToString();
        }
    }
    public void PrevLevel()
    {
        stageIcons[currentLevelIndex].Hide(true, false);
        currentLevelIndex--;
        stageIcons[currentLevelIndex].Show();
        CheckLevelBtn();
        Camera.main.DOColor(stageColors[currentLevelIndex], 1);
        foreach (var stageName in stageNames)
        {
            stageName.text = "STAGE " + (currentLevelIndex + 1).ToString();
        }
    }
}
