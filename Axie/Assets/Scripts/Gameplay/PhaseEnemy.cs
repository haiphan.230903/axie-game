using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PhaseEnemy : Phase
{
    [SerializeField] List<EnemyAppearance> enemyAppearances;
    [SerializeField] ThinGroundController thinGroundController;
    bool isEnd;
    public override void Setup()
    {
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ENEMY_PREPARE_APPEAR);
        StartCoroutine(Cor_Appear());
        isEnd = false;
        if (duration > 0) StartCoroutine(Cor_End());
        foreach(var enemyAppear in enemyAppearances)
        {
            enemyAppear.gameObject.SetActive(true);
        }
        if (thinGroundController != null)
        {
            thinGroundController.Setup();
        }
    }

    IEnumerator Cor_Appear()
    {
        yield return new WaitForSeconds(2);
        SoundManager.Instance.PlaySound(AudioPlayer.SoundID.SFX_ENEMY_APPEAR);
    }
    public override bool CheckEnd()
    {
        if (duration == 0)
        {
            foreach (EnemyAppearance e in enemyAppearances)
            {
                if (e.enemy.activeSelf || e.isDoneStarting == false) return false;
            }
            if (thinGroundController != null)
            {
                thinGroundController.HideAll();
            }
            return true;
        }
        else
        {
            return isEnd;
        }
    }
    IEnumerator Cor_End()
    {
        yield return new WaitForSeconds(duration);
        isEnd = true;
        if (thinGroundController != null)
        {
            thinGroundController.HideAll();
        }
    }    
}
