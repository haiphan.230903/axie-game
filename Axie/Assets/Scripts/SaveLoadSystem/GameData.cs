using System.Collections.Generic;
using UnityEngine;
using System;
using GDC.Configuration;
using GDC.Enums;
using GDC.Constants;

namespace GDC.Managers
{

    [Serializable]
    public struct GameData
    {
        public int currentLevelIndex; //start from 0
        public int levelIndex; //index cua level cao nhat dat duoc hien tai
        public int currentAxieIndex;
        public int AxieIndex; //index cua axie cao nhat dat duoc hien tai
        public List<int> levelStars;
        public bool isSawTutorial;
    }
}
