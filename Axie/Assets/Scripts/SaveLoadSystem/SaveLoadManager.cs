using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GDC.Managers;
using NaughtyAttributes;

namespace GDC.Managers
{
    public class SaveLoadManager : MonoBehaviour
    {
        static public SaveLoadManager Instance { get; private set; }
        public GameData GameData;
        public bool isLoadingData;

        //[SerializeField] SO_Item so_defaultArmor, so_defaultShoe;

        //public SaveLoadSystem saveLoadSystem;

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }
            Instance = this;
        }
        private void Start()
        {
            GameData.levelStars = new List<int> { 0, 0, 0, 0 };
            GameData.AxieIndex = 2;
            isLoadingData = true;
        }
    }
}
